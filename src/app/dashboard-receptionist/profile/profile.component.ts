import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { AuthGuard } from '../../auth/auth.guard';
import { LoginModel } from '../../models/login.model';
import { ReceptionistModel } from '../../models/receptionist.model';
import { DashboardReceptionistService } from '../service/dashboard-receptionist.service';
import { CepService } from '../../shared/service/cep.service';
import { RefreshTokenService } from '../../shared/service/refresh-token.service';
import { ChangePasswordSafelyComponent } from '../../shared/change-password-safely/change-password-safely.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [DashboardReceptionistService]
})
export class ProfileComponent implements OnInit {

  @ViewChild('modalChangePasswordSafely', { static: false }) public modalChangePasswordSafely: ChangePasswordSafelyComponent;

  user: LoginModel;
  receptionist: ReceptionistModel;
  selectedFile: File;
  selectedFilename: string;

  constructor(
    private authGuard: AuthGuard,
    private cepService: CepService,
    private refreshTokenService: RefreshTokenService,
    private dashboardReceptionistService: DashboardReceptionistService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
    this.receptionist = new ReceptionistModel();
  }

  ngOnInit() {
    this.user = this.authGuard.getUser();

    this.findById();
  }

  public findById() {
    this.dashboardReceptionistService.findById(this.user.id).subscribe((result: any) => {
      this.receptionist = result.receptionist;
      this.receptionist.dataNascimento = moment(this.receptionist.dataNascimento).format('DD/MM/YYYY');
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.receptionist.imagemAntiga) { fd.append('imagemAntiga', this.receptionist.imagemAntiga); }
    fd.append('id', this.receptionist.id.toString());
    fd.append('nome', this.receptionist.nome);
    fd.append('dataNascimento', this.receptionist.dataNascimento);
    fd.append('cpf', this.receptionist.cpf);
    fd.append('rg', this.receptionist.rg);
    fd.append('cep', this.receptionist.cep);
    fd.append('estado', this.receptionist.estado);
    fd.append('cidade', this.receptionist.cidade);
    fd.append('bairro', this.receptionist.bairro);
    fd.append('logradouro', this.receptionist.logradouro);
    if (this.receptionist.complemento) { fd.append('complemento', this.receptionist.complemento); }
    fd.append('numero', this.receptionist.numero);
    fd.append('celular', this.receptionist.celular);
    if (this.receptionist.telefone) { fd.append('telefone', this.receptionist.telefone); }

    this.dashboardReceptionistService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.refreshTokenService.emitEventRefreshToken();
      this.selectedFile = null;
      this.selectedFilename = '';
      this.findById();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModalChangePasswordSafely() {
    this.modalChangePasswordSafely.openModal(this.user.loginId);
  }

  public searchCep() {
    this.cepService.searchCep(this.receptionist.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.receptionist.complemento = result.complemento ? result.complemento : null;
      this.receptionist.logradouro = result.logradouro ? result.logradouro : null;
      this.receptionist.bairro = result.bairro ? result.bairro : null;
      this.receptionist.cidade = result.localidade ? result.localidade : null;
      this.receptionist.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

}
