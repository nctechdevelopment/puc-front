import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxMaskModule } from 'ngx-mask';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FullCalendarModule } from '@fullcalendar/angular';

import { DashboardReceptionistRoutingModule } from './dashboard-receptionist-routing.module';
import { DashboardReceptionistComponent } from './dashboard-receptionist.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { UpdateSchedulesComponent } from './schedules/update-schedules/update-schedules.component';
import { RegisterSchedulesComponent } from './schedules/register-schedules/register-schedules.component';
import { ConfirmEventComponent } from './schedules/confirm-event/confirm-event.component';
import { DeleteSchedulesComponent } from './schedules/delete-schedules/delete-schedules.component';

@NgModule({
  declarations: [
    DashboardReceptionistComponent,
    ProfileComponent,
    SchedulesComponent,
    UpdateSchedulesComponent,
    RegisterSchedulesComponent,
    ConfirmEventComponent,
    DeleteSchedulesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardReceptionistRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    AngularMultiSelectModule,
    FullCalendarModule,
  ]
})
export class DashboardReceptionistModule { }
