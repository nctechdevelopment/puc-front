import { Component } from '@angular/core';

import { AuthGuard } from '../auth/auth.guard';
import { LoginModel } from '../models/login.model';
import { RefreshTokenService } from '../shared/service/refresh-token.service';

@Component({
  selector: 'app-dashboard-receptionist',
  templateUrl: './dashboard-receptionist.component.html',
  styleUrls: ['./dashboard-receptionist.component.css']
})
export class DashboardReceptionistComponent {

  user: LoginModel;

  constructor(
    private authGuard: AuthGuard,
    private refreshTokenService: RefreshTokenService,
  ) {
    this.user = this.authGuard.getUser();

    this.refreshTokenService.getEventRefreshToken().subscribe(() => this.refreshToken());
  }

  public refreshToken() {
    this.refreshTokenService.refreshToken().subscribe((result: any) => {
      this.authGuard.setToken(result.token);
      this.user = this.authGuard.getUser();
    }, (error) => console.log(error));
  }

}
