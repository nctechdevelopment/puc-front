import { TestBed } from '@angular/core/testing';

import { DashboardReceptionistService } from './dashboard-receptionist.service';

describe('DashboardReceptionistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardReceptionistService = TestBed.get(DashboardReceptionistService);
    expect(service).toBeTruthy();
  });
});
