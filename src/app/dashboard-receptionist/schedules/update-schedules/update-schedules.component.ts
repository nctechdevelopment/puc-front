import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { UtilsService } from '../../../shared/service/utils.service';
import { PatientModel } from '../../../models/patient.model';
import { PatientsService } from '../../../shared/patients/service/patients.service';
import { ScheduleModel } from '../../../models/schedule.model';
import { SchedulesService } from '../service/schedules.service';
import { Settings } from '../../../shared/settings/settings';
import { DeleteSchedulesComponent } from '../delete-schedules/delete-schedules.component';

@Component({
  selector: 'app-update-schedules',
  templateUrl: './update-schedules.component.html',
  styleUrls: ['./update-schedules.component.css'],
  providers: [SchedulesService, PatientsService]
})
export class UpdateSchedulesComponent implements OnInit {

  @Output() emitDeleteSchedules: EventEmitter<any> = new EventEmitter();
  @Output() emitUpdateSchedules: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;
  @ViewChild('modalDeleteSchedules', { static: false }) public modalDeleteSchedules: DeleteSchedulesComponent;

  schedule: ScheduleModel;
  isModalShown = false;
  disableEdit = true;

  timeout = null;
  dropdownSettings = {};
  patients: Array<PatientModel>;
  selectedPatients: Array<PatientModel>;

  constructor(
    private utilsService: UtilsService,
    private patientsService: PatientsService,
    private schedulesService: SchedulesService,
    private toaster: ToasterService,
  ) {
    this.schedule = new ScheduleModel();
    this.patients = new Array<PatientModel>();
    this.selectedPatients = new Array<PatientModel>();
  }

  ngOnInit() {
    this.dropdownSettings = Object.assign({ singleSelection: true }, Settings.dropdown);
  }

  public onKeyUp(args: any) {
    const value = args.target.value.toLowerCase();

    if (value.length === 0) { return; }

    clearTimeout(this.timeout);

    if (args.keyCode === 13) {
      this.searchPatients(value);
    } else { this.timeout = setTimeout(() => this.searchPatients(value), 1000); }
  }

  public searchPatients(data: string, id?: number) {
    this.patientsService.search(data).subscribe((result: any) => {
      this.patients = [];
      result.patients.map((elem, i) => {
        this.patients[i] = {
          id: elem.id,
          info: `${elem.nome} ${elem.cpf} - Mãe: ${elem.nome_mae ? elem.nome_mae : ''}`,
          titulo: elem.nome.split(' ')[0],
          nome: elem.nome,
          nomeMae: elem.nome_mae,
          imagem: elem.imagem,
          sexo: elem.sexo,
          tipoSanguineo: elem.tipo_sanguineo,
          dataNascimento: moment(elem.data_nascimento).format('DD/MM/YYYY'),
          cpf: elem.cpf,
          rg: elem.rg,
          cns: elem.cns,
          cep: elem.cep,
          estado: elem.estado,
          cidade: elem.cidade,
          bairro: elem.bairro,
          logradouro: elem.logradouro,
          complemento: elem.complemento,
          numero: elem.numero,
          celular: elem.celular,
          telefone: elem.telefone,
          email: elem.email,
          nomeResponsavel: elem.nome_responsavel,
          cpfResponsavel: elem.cpf_responsavel,
        };
      });
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findPatientById(id: number) {
    this.patientsService.findById(id).subscribe((result: any) => {
      this.selectedPatients[0] = this.patients[0] = {
        id: result.patient.id,
        info: `${result.patient.nome} ${result.patient.cpf} - Mãe: ${result.patient.nomeMae ? result.patient.nomeMae : ''}`,
        titulo: result.patient.nome.split(' ')[0],
        nome: result.patient.nome,
        nomeMae: result.patient.nomeMae,
        imagem: result.patient.imagem,
        sexo: result.patient.sexo,
        tipoSanguineo: result.patient.tipoSanguineo,
        dataNascimento: moment(result.patient.dataNascimento).format('DD/MM/YYYY'),
        cpf: result.patient.cpf,
        rg: result.patient.rg,
        cns: result.patient.cns,
        cep: result.patient.cep,
        estado: result.patient.estado,
        cidade: result.patient.cidade,
        bairro: result.patient.bairro,
        logradouro: result.patient.logradouro,
        complemento: result.patient.complemento,
        numero: result.patient.numero,
        celular: result.patient.celular,
        telefone: result.patient.telefone,
        email: result.patient.email,
        nomeResponsavel: result.patient.nomeResponsavel,
        cpfResponsavel: result.patient.cpfResponsavel,
      };
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    this.schedule.titulo = this.selectedPatients[0].titulo;
    this.schedule.paciente.id = this.selectedPatients[0].id;
    this.schedulesService.update(this.schedule).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateSchedules.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(schedule: ScheduleModel) {
    this.schedule = Object.assign({}, schedule);
    this.handleDisableEdit(this.schedule);
    this.findPatientById(this.schedule.paciente.id);
    this.isModalShown = true;
  }

  public closeModal() {
    this.patients = [];
    this.selectedPatients = [];
    this.schedule = new ScheduleModel();
    this.disableEdit = true;
    this.autoShownModal.hide();
  }

  public openModalDeleteSchedules() {
    this.modalDeleteSchedules.openModal(this.schedule.id);
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public onDeSelectAll() {
    this.selectedPatients = [];
  }

  public handleDisableEdit(args: any) {
    const date = args.dataInicio.split(' ')[0].split('/').reverse().join('-');
    if (args.status !== '0') {
      this.disableEdit = true;
    } else if (this.utilsService.subtractingDatesInDays(date) < 0) {
      this.disableEdit = true;
    } else { this.disableEdit = false; }
  }

  public subscriberSchedules() {
    this.closeModal();
    this.emitDeleteSchedules.emit();
  }

}
