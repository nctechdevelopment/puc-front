import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import ptBr from '@fullcalendar/core/locales/pt-br';

import { UtilsService } from '../../shared/service/utils.service';
import { DoctorModel } from '../../models/doctor.model';
import { ScheduleModel } from '../../models/schedule.model';
import { SchedulesService } from './service/schedules.service';
import { ConfirmEventComponent } from './confirm-event/confirm-event.component';
import { RegisterSchedulesComponent } from './register-schedules/register-schedules.component';
import { UpdateSchedulesComponent } from './update-schedules/update-schedules.component';

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.css'],
  providers: [SchedulesService]
})
export class SchedulesComponent implements OnInit {

  @ViewChild('modalConfirmEvent', { static: false }) public modalConfirmEvent: ConfirmEventComponent;
  @ViewChild('modalRegisterSchedules', { static: false }) public modalRegisterSchedules: RegisterSchedulesComponent;
  @ViewChild('modalUpdateSchedules', { static: false }) public modalUpdateSchedules: UpdateSchedulesComponent;
  @ViewChild('calendar', { static: false }) calendar: FullCalendarComponent;

  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin, listPlugin];
  calendarEvents: EventInput;
  locales = [ptBr];

  doctorId = 0;
  showingDoctorId = 0;
  doctors: DoctorModel;
  schedule: ScheduleModel;
  updateStartDate: ScheduleModel;
  updateEndDate: ScheduleModel;
  argsEvent: any;
  businessHours = [{
    daysOfWeek: [1, 2, 3, 4, 5],
    startTime: '07:00',
    endTime: '12:00',
  }, {
    daysOfWeek: [1, 2, 3, 4, 5],
    startTime: '13:00',
    endTime: '23:30',
  },
  {
    daysOfWeek: [6],
    startTime: '07:00',
    endTime: '12:00',
  }];

  constructor(
    private utilsService: UtilsService,
    private schedulesService: SchedulesService,
    private toaster: ToasterService,
  ) {
    this.updateStartDate = new ScheduleModel();
    this.updateEndDate = new ScheduleModel();
  }

  ngOnInit() {
    this.findAllDoctors();
  }

  public findAllDoctors() {
    this.schedulesService.findAllDoctors().subscribe((result: any) => {
      this.doctors = result.doctors;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findAllEvent = (dates, callback) => {
    const start = moment(dates.start).format('YYYY-MM-DD');
    const end = moment(dates.end).format('YYYY-MM-DD');
    this.schedulesService.findAllByDoctor(this.doctorId, start, end).subscribe((result: any) => {
      this.showingDoctorId = this.doctorId;
      this.calendarEvents = result.schedules.map(elem => {
        return {
          id: elem.id,
          title: `${elem.titulo} (${elem.tipo})`,
          start: elem.dataInicio,
          end: elem.dataFim,
          classNames: this.utilsService.getClassToStatus(elem.status),
          extendedProps: {
            id: elem.id,
            title: `${elem.titulo} (${elem.tipo})`,
            titulo: elem.titulo,
            paciente: { id: elem.paciente.id },
            medico: { id: elem.medico.id },
            tipo: elem.tipo,
            status: elem.status,
            dataInicio: moment(elem.dataInicio).format('DD/MM/YYYY HH:mm:ss'),
            dataFim: moment(elem.dataFim).format('DD/MM/YYYY HH:mm:ss'),
            data: elem.dataAgendamento
          },
        };
      });

      callback(this.calendarEvents);
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public updateEvent() {
    const updateEndDate = Object.assign(this.updateEndDate, {
      dataInicio: moment(this.updateEndDate.dataInicio).format('YYYY-MM-DD HH:mm:ss'),
      dataFim: moment(this.updateEndDate.dataFim).format('YYYY-MM-DD HH:mm:ss'),
    });
    this.schedulesService.updateEventDrop(updateEndDate).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.refetchEvents();
    }, (error) => {
      console.log(error);
      this.argsEvent.revert();
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  // Events
  public handleDateClick(args: any) {
    const date = moment(args.start).format('YYYY-MM-DD');
    if (this.doctorId === 0) {
      this.toaster.pop('info', 'Ops!', 'Selecione um médico!');
    } else if (this.utilsService.subtractingDatesInDays(date) < 0) {
      this.toaster.pop('warning', 'Ops!', 'Não é possível fazer agendamentos em horários antigos!');
    } else { this.openModalRegisterSchedules(args); }
  }

  public handleEventClick(args: any) {
    this.openModalUpdateSchedules(args.event.extendedProps);
  }

  public handleEventAllow = (args: any) => {
    const date = moment(args.start).format('YYYY-MM-DD');
    const startDate = moment(this.updateStartDate.dataInicio).format('YYYY-MM-DD');
    if (this.utilsService.subtractingDatesInDays(startDate) < 0) {
      this.toaster.pop('warning', 'Ops!', 'Não é possível alterar agendamentos antigos!');
      return false;
    } else if (this.utilsService.subtractingDatesInDays(date) < 0) {
      this.toaster.pop('warning', 'Ops!', 'Não é possível fazer agendamentos em horários antigos!');
      return false;
    } else { return true; }
  }

  public handleDragStart(args: any) {
    const status = args.event.extendedProps.status;
    if (status === '1' || status === '2') {
      this.toaster.pop('warning', 'Ops!', 'Não é possível alterar agendamentos realizados!');
    } else {
      this.updateStartDate.id = args.event.id;
      this.updateStartDate.dataInicio = args.event.start;
      this.updateStartDate.dataFim = args.event.end;
    }
  }

  public handleDrop(args: any) {
    const status = args.event.extendedProps.status;
    if (status === '1' || status === '2') {
      this.toaster.pop('warning', 'Ops!', 'Não é possível alterar agendamentos realizados!');
      args.revert();
    } else {
      this.argsEvent = args;
      this.updateEndDate.id = args.event.id;
      this.updateEndDate.paciente.id = args.event.extendedProps.paciente.id;
      this.updateEndDate.medico.id = args.event.extendedProps.medico.id;
      this.updateEndDate.dataInicio = args.event.start;
      this.updateEndDate.dataFim = args.event.end;

      this.openModalConfirmEvent();
    }
  }

  public handleResizeStart(args: any) {
    const status = args.event.extendedProps.status;
    if (status === '1' || status === '2') {
      this.toaster.pop('warning', 'Ops!', 'Não é possível alterar agendamentos realizados!');
    } else {
      this.updateStartDate.id = args.event.id;
      this.updateStartDate.dataInicio = args.event.start;
      this.updateStartDate.dataFim = args.event.end;
    }
  }

  public handleResize(args: any) {
    const status = args.event.extendedProps.status;
    if (status === '1' || status === '2') {
      this.toaster.pop('warning', 'Ops!', 'Não é possível alterar agendamentos realizados!');
      args.revert();
    } else {
      this.argsEvent = args;
      this.updateEndDate.id = args.event.id;
      this.updateEndDate.paciente.id = args.event.extendedProps.paciente.id;
      this.updateEndDate.medico.id = args.event.extendedProps.medico.id;
      this.updateEndDate.dataInicio = args.event.start;
      this.updateEndDate.dataFim = args.event.end;

      this.openModalConfirmEvent();
    }
  }

  public refetchEvents() {
    const myCalendar = this.calendar.getApi();
    myCalendar.refetchEvents();
  }

  public showSchedulesByDoctor(id: number) {
    this.doctorId = id;
    this.refetchEvents();
  }

  // Modals
  public subscriberConfirmUpdateSchedules(confirmation: boolean) {
    confirmation ? this.updateEvent() : this.argsEvent.revert();
  }

  public subscriberSchedules() {
    this.refetchEvents();
  }

  public openModalConfirmEvent() {
    this.modalConfirmEvent.openModal(this.updateStartDate, this.updateEndDate);
  }

  public openModalRegisterSchedules(args: any) {
    const dataInicio = moment(args.start).format('DD/MM/YYYY HH:mm:ss');
    const dataFim = moment(args.end).format('DD/MM/YYYY HH:mm:ss');
    this.modalRegisterSchedules.openModal(dataInicio, dataFim, this.doctorId);
  }

  public openModalUpdateSchedules(args: any) {
    this.modalUpdateSchedules.openModal(args);
  }

}
