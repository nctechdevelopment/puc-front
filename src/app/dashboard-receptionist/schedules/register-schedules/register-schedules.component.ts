import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { PatientModel } from '../../../models/patient.model';
import { PatientsService } from '../../../shared/patients/service/patients.service';
import { ScheduleModel } from '../../../models/schedule.model';
import { SchedulesService } from '../service/schedules.service';
import { Settings } from '../../../shared/settings/settings';

@Component({
  selector: 'app-register-schedules',
  templateUrl: './register-schedules.component.html',
  styleUrls: ['./register-schedules.component.css'],
  providers: [SchedulesService, PatientsService]
})
export class RegisterSchedulesComponent implements OnInit {

  @Output() emitRegisterSchedules: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  doctorId: number;
  schedule: ScheduleModel;
  isModalShown = false;

  timeout = null;
  dropdownSettings = {};
  patients: Array<PatientModel>;
  selectedPatients: Array<PatientModel>;

  constructor(
    private patientsService: PatientsService,
    private schedulesService: SchedulesService,
    private toaster: ToasterService,
  ) {
    this.schedule = new ScheduleModel();
    this.patients = new Array<PatientModel>();
    this.selectedPatients = new Array<PatientModel>();
  }

  ngOnInit() {
    this.dropdownSettings = Object.assign({ singleSelection: true }, Settings.dropdown);
  }

  public onKeyUp(args: any) {
    const value = args.target.value.toLowerCase();

    if (value.length === 0) { return; }

    clearTimeout(this.timeout);

    if (args.keyCode === 13) {
      this.searchPatients(value);
    } else { this.timeout = setTimeout(() => this.searchPatients(value), 1000); }
  }

  public searchPatients(data: string) {
    this.patientsService.search(data).subscribe((result: any) => {
      this.patients = [];
      result.patients.map((elem, i) => {
        this.patients[i] = {
          id: elem.id,
          info: `${elem.nome} ${elem.cpf} - Mãe: ${elem.nome_mae ? elem.nome_mae : ''}`,
          titulo: elem.nome.split(' ')[0],
          nome: elem.nome,
          nomeMae: elem.nome_mae,
          imagem: elem.imagem,
          sexo: elem.sexo,
          tipoSanguineo: elem.tipo_sanguineo,
          dataNascimento: moment(elem.data_nascimento).format('DD/MM/YYYY'),
          cpf: elem.cpf,
          rg: elem.rg,
          cns: elem.cns,
          cep: elem.cep,
          estado: elem.estado,
          cidade: elem.cidade,
          bairro: elem.bairro,
          logradouro: elem.logradouro,
          complemento: elem.complemento,
          numero: elem.numero,
          celular: elem.celular,
          telefone: elem.telefone,
          email: elem.email,
          nomeResponsavel: elem.nome_responsavel,
          cpfResponsavel: elem.cpf_responsavel,
        };
      });
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public register() {
    this.schedule.medico.id = this.doctorId;
    this.schedule.titulo = this.selectedPatients[0].titulo;
    this.schedule.paciente.id = this.selectedPatients[0].id;
    this.schedulesService.register(this.schedule).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitRegisterSchedules.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(start: string, end: string, id: number) {
    this.schedule.dataInicio = start;
    this.schedule.dataFim = end;
    this.doctorId = id;
    this.isModalShown = true;
  }

  public closeModal() {
    this.patients = [];
    this.selectedPatients = [];
    this.schedule = new ScheduleModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public onDeSelectAll() {
    this.selectedPatients = [];
  }

}
