import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { SchedulesService } from '../service/schedules.service';

@Component({
  selector: 'app-delete-schedules',
  templateUrl: './delete-schedules.component.html',
  styleUrls: ['./delete-schedules.component.css']
})
export class DeleteSchedulesComponent {

  @Output() emitDeleteSchedules: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  scheduleId: number;
  isModalShown = false;

  constructor(
    private schedulesService: SchedulesService,
    private toaster: ToasterService,
  ) { }

  public delete() {
    this.schedulesService.delete(this.scheduleId).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitDeleteSchedules.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public confirmEvent() {
    this.delete();
    this.closeModal();
  }

  public openModal(id: number) {
    this.scheduleId = id;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
