import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { PermissionGuard } from '../auth/permission.guard';
import { DashboardReceptionistComponent } from './dashboard-receptionist.component';
import { ProfileComponent } from './profile/profile.component';
import { SchedulesComponent } from '../dashboard-receptionist/schedules/schedules.component';

// Patients
import { ListPatientsComponent } from '../shared/patients/list-patients/list-patients.component';

const routes: Routes = [
  {
    path: 'painel-recepcionista',
    canActivate: [AuthGuard, PermissionGuard],
    data: { roles: ['4'] },
    component: DashboardReceptionistComponent,
    children: [
      {
        path: 'agenda',
        canActivate: [PermissionGuard],
        data: { roles: ['4'] },
        component: SchedulesComponent
      },
      {
        path: 'listar/pacientes',
        canActivate: [PermissionGuard],
        data: { roles: ['4'] },
        component: ListPatientsComponent
      },
      {
        path: 'perfil',
        canActivate: [PermissionGuard],
        data: { roles: ['4'] },
        component: ProfileComponent
      },
    ],
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardReceptionistRoutingModule { }
