import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { MedicamentModel } from '../../../models/medicament.model';

@Injectable({
  providedIn: 'root'
})
export class MedicamentsService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/medicament`;
  }

  searchAll(data: any): Observable<any> {
    return this.http.post(`${this.url}/search`, data).pipe(map((result: Array<MedicamentModel>) => result));
  }

  register(data: MedicamentModel): Observable<any> {
    return this.http.post(`${this.url}/register`, data).pipe(map((result: any) => result));
  }

  update(data: MedicamentModel): Observable<any> {
    return this.http.put(`${this.url}/update/${data.id}`, data).pipe(map((result: any) => result));
  }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`).pipe(map((result: any) => result));
  }

  updateStatus(id: number, status: string): Observable<any> {
    return this.http.put(`${this.url}/${id}/status/${status}`, {}).pipe(map((result: any) => result));
  }

}
