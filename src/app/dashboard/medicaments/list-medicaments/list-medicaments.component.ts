import { Component, AfterViewInit, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';

import { MedicamentModel } from '../../../models/medicament.model';
import { MedicamentsService } from '../service/medicaments.service';
import { RegisterMedicamentsComponent } from '../register-medicaments/register-medicaments.component';
import { UpdateMedicamentsComponent } from '../update-medicaments/update-medicaments.component';
import { UpdateStatusMedicamentsComponent } from '../update-status-medicaments/update-status-medicaments.component';

@Component({
  selector: 'app-list-medicaments',
  templateUrl: './list-medicaments.component.html',
  styleUrls: ['./list-medicaments.component.css'],
  providers: [MedicamentsService]
})
export class ListMedicamentsComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('modalRegisterMedicaments', { static: false }) public modalRegisterMedicaments: RegisterMedicamentsComponent;
  @ViewChild('modalUpdateMedicaments', { static: false }) public modalUpdateMedicaments: UpdateMedicamentsComponent;
  @ViewChild('modalUpdateStatusMedicaments', { static: false }) public modalUpdateStatusMedicaments: UpdateStatusMedicamentsComponent;

  page = 1;
  medicaments: Array<MedicamentModel>;
  dtOptions: any = {};
  listenClickDownload: any;

  constructor(
    private renderer: Renderer2,
    private medicamentsService: MedicamentsService,
    private toaster: ToasterService,
  ) {
    this.medicaments = new Array<MedicamentModel>();
  }

  ngOnInit() {
    this.dtOptions = {
      autoWidth: false,
      responsive: true,
      serverSide: true,
      processing: true,
      searchDelay: 1000,
      ordering: false,
      pageLength: 25,
      pagingType: 'full_numbers',
      table: '#table-medicaments',
      dom: 'Bfrtip',
      buttons: [
        'pageLength',
        'colvis',
      ],
      columns: [
        {
          name: 'Nome genérico',
          data: 'nome_generico'
        },
        {
          name: 'Nome de fábrica',
          data: 'nome_fabrica'
        },
        {
          name: 'Nome do fabricante',
          data: 'nome_fabricante'
        },
        {
          name: 'Ações',
          data: 'id',
          render: (data, type, row) => {
            return `
              <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-sm" data-edit-medicament-id="${data}">
                  <i class="fas fa-edit" data-edit-medicament-id="${data}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-update-status-medicament-id="${data}">
                  ${
                    row.status === '1'
                    ? `<i class="fas fa-check-circle" data-update-status-medicament-id="${data}"
                      data-update-status-medicament-status="${row.status}"></i>`
                    : `<i class="fas fa-times-circle" data-update-status-medicament-id="${data}"
                      data-update-status-medicament-status="${row.status}"></i>`
                  }
                </button>
              </div>
            `;
          },
        },
      ],
      language: {
        processing: 'Procesando...',
        search: 'Pesquisar:',
        lengthMenu: '_MENU_',
        info: 'Mostrando _START_ até _END_ de _TOTAL_ registros',
        infoEmpty: 'Mostrando 0 até 0 de 0 registros',
        infoFiltered: '(Filtrados de _MAX_ registros)',
        infoPostFix: '',
        sInfoThousands: '.',
        loadingRecords: 'Carregando...',
        zeroRecords: 'Nenhum registro encontrado',
        emptyTable: 'Nenhum registro encontrado',
        paginate: {
          next: 'Próximo',
          previous: 'Anterior',
          first: 'Primeiro',
          last: 'Último'
        },
        aria: {
          sortAscending: ': Ordenar colunas de forma ascendente',
          sortDescending: ': Ordenar colunas de forma descendente'
        },
        buttons: {
          colvis: 'Colunas visíveis',
          pageLength: {
            _: 'Mostrar %d resultados',
          }
        },
      },
      fnServerParams: () => {
        const table = $('#table-medicaments').DataTable();

        table.on('page.dt', () => {
          const info = table.page.info();
          this.page = info.page + 1;
        });

        table.on('search.dt', () => this.page = 1);
      },
    };

    this.searchAll();
  }

  ngOnDestroy() {
    this.listenClickDownload();
    this.renderer.destroy();
  }

  public searchAll() {
    this.dtOptions.ajax = (dataTablesParameters: any, callback) => {
      this.medicamentsService.searchAll({
        page: this.page,
        search: dataTablesParameters.search.value,
        length: dataTablesParameters.length,
      }).subscribe((result: any) => {
        callback({
          recordsTotal: result.recordsTotal,
          recordsFiltered: result.recordsFiltered,
          data: result.medicaments
        });
      }, (error) => {
        console.log(error);
        this.toaster.pop('error', 'Ops!', error.error.message);
      });
    };
  }

  public dtSearch() {
    this.page = 1;

    $('#table-medicaments').DataTable().ajax.reload();
  }

  public ngAfterViewInit(): void {
    this.listenClickDownload = this.renderer.listen('document', 'click', (event) => {
      if (event.target.hasAttribute('data-edit-medicament-id')) {
        this.openModalUpdateMedicaments(event.target.getAttribute('data-edit-medicament-id'));
      } else if (event.target.hasAttribute('data-update-status-medicament-id')) {
        this.openModalUpdateStatusMedicaments(
          event.target.getAttribute('data-update-status-medicament-id'),
          event.target.getAttribute('data-update-status-medicament-status')
        );
      }
    });
  }

  public subscriberMedicaments() {
    this.dtSearch();
  }

  public openModalRegisterMedicaments() {
    this.modalRegisterMedicaments.openModal();
  }

  public openModalUpdateMedicaments(id: number) {
    this.modalUpdateMedicaments.openModal(id);
  }

  public openModalUpdateStatusMedicaments(id: number, status: string) {
    this.modalUpdateStatusMedicaments.openModal(id, status);
  }

}
