import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMedicamentsComponent } from './update-medicaments.component';

describe('UpdateMedicamentsComponent', () => {
  let component: UpdateMedicamentsComponent;
  let fixture: ComponentFixture<UpdateMedicamentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateMedicamentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMedicamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
