import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { MedicamentModel } from '../../../models/medicament.model';
import { MedicamentsService } from '../service/medicaments.service';

@Component({
  selector: 'app-update-medicaments',
  templateUrl: './update-medicaments.component.html',
  styleUrls: ['./update-medicaments.component.css'],
  providers: [MedicamentsService]
})
export class UpdateMedicamentsComponent implements OnInit {

  @Output() emitUpdateMedicaments: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  medicament: MedicamentModel;
  isModalShown = false;

  constructor(
    private medicamentsService: MedicamentsService,
    private toaster: ToasterService,
  ) {
    this.medicament = new MedicamentModel();
  }

  ngOnInit() {
  }

  public findById(id: number) {
    this.medicamentsService.findById(id).subscribe((result: any) => {
      this.medicament = result.medicament;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    this.medicamentsService.update(this.medicament).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateMedicaments.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number) {
    this.findById(id);
    this.isModalShown = true;
  }

  public closeModal() {
    this.medicament = new MedicamentModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
