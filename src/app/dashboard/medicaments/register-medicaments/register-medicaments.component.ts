import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { MedicamentModel } from '../../../models/medicament.model';
import { MedicamentsService } from '../service/medicaments.service';

@Component({
  selector: 'app-register-medicaments',
  templateUrl: './register-medicaments.component.html',
  styleUrls: ['./register-medicaments.component.css'],
  providers: [MedicamentsService]
})
export class RegisterMedicamentsComponent {

  @Output() emitRegisterMedicaments: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  medicament: MedicamentModel;
  isModalShown = false;

  constructor(
    private medicamentsService: MedicamentsService,
    private toaster: ToasterService,
  ) {
    this.medicament = new MedicamentModel();
  }

  public register() {
    this.medicamentsService.register(this.medicament).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitRegisterMedicaments.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal() {
    this.isModalShown = true;
  }

  public closeModal() {
    this.medicament = new MedicamentModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
