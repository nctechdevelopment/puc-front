import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusMedicamentsComponent } from './update-status-medicaments.component';

describe('UpdateStatusMedicamentsComponent', () => {
  let component: UpdateStatusMedicamentsComponent;
  let fixture: ComponentFixture<UpdateStatusMedicamentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateStatusMedicamentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusMedicamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
