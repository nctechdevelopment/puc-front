import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { MedicamentsService } from '../service/medicaments.service';

@Component({
  selector: 'app-update-status-medicaments',
  templateUrl: './update-status-medicaments.component.html',
  styleUrls: ['./update-status-medicaments.component.css'],
  providers: [MedicamentsService]
})
export class UpdateStatusMedicamentsComponent {

  @Output() emitUpdateStatusMedicaments: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  id: number;
  status: string;
  isModalShown = false;

  constructor(
    private medicamentsService: MedicamentsService,
    private toaster: ToasterService,
  ) { }

  public updateStatus() {
    this.medicamentsService.updateStatus(this.id, (this.status === '1' ? '0' : '1')).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateStatusMedicaments.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number, status: string) {
    this.id = id;
    this.status = status;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
