import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { PermissionGuard } from '../auth/permission.guard';
import { DashboardComponent } from './dashboard.component';

// Laboratories
import { ListLaboratoriesComponent } from './laboratories/list-laboratories/list-laboratories.component';
// Doctors
import { ListDoctorsComponent } from './doctors/list-doctors/list-doctors.component';
// Receptionists
import { ListReceptionistsComponent } from './receptionists/list-receptionists/list-receptionists.component';
// Patients
import { ListPatientsComponent } from '../shared/patients/list-patients/list-patients.component';
// Medicaments
import { ListMedicamentsComponent } from './medicaments/list-medicaments/list-medicaments.component';

const routes: Routes = [
  {
    path: 'painel-admin',
    canActivate: [AuthGuard, PermissionGuard],
    data: { roles: ['1'] },
    component: DashboardComponent,
    children: [
      {
        path: 'listar/laboratorios',
        canActivate: [PermissionGuard],
        data: { roles: ['1'] },
        component: ListLaboratoriesComponent
      },
      {
        path: 'listar/medicos',
        canActivate: [PermissionGuard],
        data: { roles: ['1'] },
        component: ListDoctorsComponent
      },
      {
        path: 'listar/recepcionistas',
        canActivate: [PermissionGuard],
        data: { roles: ['1'] },
        component: ListReceptionistsComponent
      },
      {
        path: 'listar/pacientes',
        canActivate: [PermissionGuard],
        data: { roles: ['1'] },
        component: ListPatientsComponent
      },
      {
        path: 'listar/medicamentos',
        canActivate: [PermissionGuard],
        data: { roles: ['1'] },
        component: ListMedicamentsComponent
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
