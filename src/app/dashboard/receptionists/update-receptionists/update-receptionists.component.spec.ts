import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReceptionistsComponent } from './update-receptionists.component';

describe('UpdateReceptionistsComponent', () => {
  let component: UpdateReceptionistsComponent;
  let fixture: ComponentFixture<UpdateReceptionistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReceptionistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReceptionistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
