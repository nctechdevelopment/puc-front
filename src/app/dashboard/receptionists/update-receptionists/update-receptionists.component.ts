import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { ReceptionistModel } from '../../../models/receptionist.model';
import { ReceptionistsService } from '../service/receptionists.service';
import { CepService } from '../../../shared/service/cep.service';

@Component({
  selector: 'app-update-receptionists',
  templateUrl: './update-receptionists.component.html',
  styleUrls: ['./update-receptionists.component.css'],
  providers: [ReceptionistsService]
})
export class UpdateReceptionistsComponent implements OnInit {

  @Output() emitUpdateReceptionists: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  receptionist: ReceptionistModel;
  selectedFile: File;
  selectedFilename: string;
  isModalShown = false;

  constructor(
    private cepService: CepService,
    private receptionistsService: ReceptionistsService,
    private toaster: ToasterService,
  ) {
    this.receptionist = new ReceptionistModel();
  }

  ngOnInit() {
  }

  public findById(id: number) {
    this.receptionistsService.findById(id).subscribe((result: any) => {
      this.receptionist = result.receptionist;
      this.receptionist.dataNascimento = moment(this.receptionist.dataNascimento).format('DD/MM/YYYY');
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.receptionist.imagemAntiga) { fd.append('imagemAntiga', this.receptionist.imagemAntiga); }
    fd.append('id', this.receptionist.id.toString());
    fd.append('nome', this.receptionist.nome);
    fd.append('dataNascimento', this.receptionist.dataNascimento);
    fd.append('cpf', this.receptionist.cpf);
    fd.append('rg', this.receptionist.rg);
    fd.append('cep', this.receptionist.cep);
    fd.append('estado', this.receptionist.estado);
    fd.append('cidade', this.receptionist.cidade);
    fd.append('bairro', this.receptionist.bairro);
    fd.append('logradouro', this.receptionist.logradouro);
    if (this.receptionist.complemento) { fd.append('complemento', this.receptionist.complemento); }
    fd.append('numero', this.receptionist.numero);
    fd.append('celular', this.receptionist.celular);
    if (this.receptionist.telefone) { fd.append('telefone', this.receptionist.telefone); }

    this.receptionistsService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateReceptionists.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number) {
    this.findById(id);
    this.isModalShown = true;
  }

  public closeModal() {
    this.selectedFile = null;
    this.selectedFilename = '';
    this.receptionist = new ReceptionistModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public searchCep() {
    this.cepService.searchCep(this.receptionist.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.receptionist.complemento = result.complemento ? result.complemento : null;
      this.receptionist.logradouro = result.logradouro ? result.logradouro : null;
      this.receptionist.bairro = result.bairro ? result.bairro : null;
      this.receptionist.cidade = result.localidade ? result.localidade : null;
      this.receptionist.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

}
