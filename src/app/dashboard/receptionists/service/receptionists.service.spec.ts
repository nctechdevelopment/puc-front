import { TestBed } from '@angular/core/testing';

import { ReceptionistsService } from './receptionists.service';

describe('ReceptionistsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReceptionistsService = TestBed.get(ReceptionistsService);
    expect(service).toBeTruthy();
  });
});
