import { Component, AfterViewInit, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { ReceptionistModel } from '../../../models/receptionist.model';
import { ReceptionistsService } from '../service/receptionists.service';
import { RegisterReceptionistsComponent } from '../register-receptionists/register-receptionists.component';
import { UpdateReceptionistsComponent } from '../update-receptionists/update-receptionists.component';
import { UpdateStatusReceptionistsComponent } from '../update-status-receptionists/update-status-receptionists.component';
import { ChangePasswordComponent } from '../../../shared/change-password/change-password.component';

@Component({
  selector: 'app-list-receptionists',
  templateUrl: './list-receptionists.component.html',
  styleUrls: ['./list-receptionists.component.css'],
  providers: [ReceptionistsService]
})
export class ListReceptionistsComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('modalRegisterReceptionists', { static: false }) public modalRegisterReceptionists: RegisterReceptionistsComponent;
  @ViewChild('modalUpdateReceptionists', { static: false }) public modalUpdateReceptionists: UpdateReceptionistsComponent;
  @ViewChild('modalUpdateStatusReceptionists', { static: false }) public modalUpdateStatusReceptionists: UpdateStatusReceptionistsComponent;
  @ViewChild('modalChangePassword', { static: false }) public modalChangePassword: ChangePasswordComponent;

  page = 1;
  receptionists: Array<ReceptionistModel>;
  dtOptions: any = {};
  listenClickDownload: any;

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private receptionistsService: ReceptionistsService,
    private toaster: ToasterService,
  ) {
    this.receptionists = new Array<ReceptionistModel>();
  }

  ngOnInit() {
    this.dtOptions = {
      autoWidth: false,
      responsive: true,
      serverSide: true,
      processing: true,
      searchDelay: 1000,
      ordering: false,
      pageLength: 25,
      pagingType: 'full_numbers',
      table: '#table-receptionists',
      dom: 'Bfrtip',
      buttons: [
        'pageLength',
        'colvis',
      ],
      columns: [
        {
          name: 'Nome',
          data: 'nome'
        },
        {
          name: 'Data de nascimento',
          data: 'data_nascimento',
          render: (data) => {
            if (moment(data).isValid()) {
              return moment(data).format('DD/MM/YYYY');
            } else { return ''; }
          }
        },
        {
          name: 'CPF',
          data: 'cpf'
        },
        {
          name: 'RG',
          data: 'rg'
        },
        {
          name: 'Celular',
          data: 'celular'
        },
        {
          name: 'Telefone',
          data: 'telefone'
        },
        {
          name: 'E-mail',
          data: 'email'
        },
        {
          name: 'Endereço completo',
          render: (data, type, row) => {
            return `
              ${row.logradouro}, ${row.numero}, ${row.bairro} -
              ${row.cidade}, ${row.estado}, ${row.cep}
            ` + (row.complemento ? `(${row.complemento})` : '');
          }
        },
        {
          name: 'Ações',
          data: 'id',
          render: (data, type, row) => {
            return `
              <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-sm" data-edit-receptionist-id="${data}">
                  <i class="fas fa-edit" data-edit-receptionist-id="${data}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-change-password-login-id="${row.login_id}">
                  <i class="fas fa-key" data-change-password-login-id="${row.login_id}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-update-status-receptionist-id="${data}">
                  ${
                    row.status === '1'
                    ? `<i class="fas fa-check-circle" data-update-status-receptionist-id="${data}"
                      data-update-status-receptionist-status="${row.status}"></i>`
                    : `<i class="fas fa-times-circle" data-update-status-receptionist-id="${data}"
                      data-update-status-receptionist-status="${row.status}"></i>`
                  }
                </button>
              </div>
            `;
          },
        },
      ],
      columnDefs: [
        {
          targets: [1, 3, 7],
          visible: false,
        },
      ],
      language: {
        processing: 'Procesando...',
        search: 'Pesquisar:',
        lengthMenu: '_MENU_',
        info: 'Mostrando _START_ até _END_ de _TOTAL_ registros',
        infoEmpty: 'Mostrando 0 até 0 de 0 registros',
        infoFiltered: '(Filtrados de _MAX_ registros)',
        infoPostFix: '',
        sInfoThousands: '.',
        loadingRecords: 'Carregando...',
        zeroRecords: 'Nenhum registro encontrado',
        emptyTable: 'Nenhum registro encontrado',
        paginate: {
          next: 'Próximo',
          previous: 'Anterior',
          first: 'Primeiro',
          last: 'Último'
        },
        aria: {
          sortAscending: ': Ordenar colunas de forma ascendente',
          sortDescending: ': Ordenar colunas de forma descendente'
        },
        buttons: {
          colvis: 'Colunas visíveis',
          pageLength: {
            _: 'Mostrar %d resultados',
          }
        },
      },
      fnServerParams: () => {
        const table = $('#table-receptionists').DataTable();

        table.on('page.dt', () => {
          const info = table.page.info();
          this.page = info.page + 1;
        });

        table.on('search.dt', () => this.page = 1);
      },
    };

    this.searchAll();
  }

  ngOnDestroy() {
    this.listenClickDownload();
    this.renderer.destroy();
  }

  public searchAll() {
    this.dtOptions.ajax = (dataTablesParameters: any, callback) => {
      this.receptionistsService.searchAll({
        page: this.page,
        search: dataTablesParameters.search.value,
        length: dataTablesParameters.length,
      }).subscribe((result: any) => {
        callback({
          recordsTotal: result.recordsTotal,
          recordsFiltered: result.recordsFiltered,
          data: result.receptionists
        });
      }, (error) => {
        console.log(error);
        this.toaster.pop('error', 'Ops!', error.error.message);
      });
    };
  }

  public dtSearch() {
    this.page = 1;

    $('#table-receptionists').DataTable().ajax.reload();
  }

  public ngAfterViewInit(): void {
    this.listenClickDownload = this.renderer.listen('document', 'click', (event) => {
      if (event.target.hasAttribute('data-edit-receptionist-id')) {
        this.openModalUpdateReceptionists(event.target.getAttribute('data-edit-receptionist-id'));
      } else if (event.target.hasAttribute('data-update-status-receptionist-id')) {
        this.openModalUpdateStatusReceptionists(
          event.target.getAttribute('data-update-status-receptionist-id'),
          event.target.getAttribute('data-update-status-receptionist-status')
        );
      } else if (event.target.hasAttribute('data-change-password-login-id')) {
        this.openModalChangePassword(event.target.getAttribute('data-change-password-login-id'));
      }
    });
  }

  public subscriberReceptionists() {
    this.dtSearch();
  }

  public openModalRegisterReceptionists() {
    this.modalRegisterReceptionists.openModal();
  }

  public openModalUpdateReceptionists(id: number) {
    this.modalUpdateReceptionists.openModal(id);
  }

  public openModalUpdateStatusReceptionists(id: number, status: string) {
    this.modalUpdateStatusReceptionists.openModal(id, status);
  }

  public openModalChangePassword(loginId: number) {
    this.modalChangePassword.openModal(loginId);
  }

}
