import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { ReceptionistsService } from '../service/receptionists.service';

@Component({
  selector: 'app-update-status-receptionists',
  templateUrl: './update-status-receptionists.component.html',
  styleUrls: ['./update-status-receptionists.component.css'],
  providers: [ReceptionistsService]
})
export class UpdateStatusReceptionistsComponent {

  @Output() emitUpdateStatusReceptionists: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  id: number;
  status: string;
  isModalShown = false;

  constructor(
    private receptionistsService: ReceptionistsService,
    private toaster: ToasterService,
  ) { }

  public updateStatus() {
    this.receptionistsService.updateStatus(this.id, (this.status === '1' ? '0' : '1')).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateStatusReceptionists.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number, status: string) {
    this.id = id;
    this.status = status;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
