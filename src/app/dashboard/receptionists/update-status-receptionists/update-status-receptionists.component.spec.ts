import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusReceptionistsComponent } from './update-status-receptionists.component';

describe('UpdateStatusReceptionistsComponent', () => {
  let component: UpdateStatusReceptionistsComponent;
  let fixture: ComponentFixture<UpdateStatusReceptionistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateStatusReceptionistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusReceptionistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
