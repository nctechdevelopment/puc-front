import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterReceptionistsComponent } from './register-receptionists.component';

describe('RegisterReceptionistsComponent', () => {
  let component: RegisterReceptionistsComponent;
  let fixture: ComponentFixture<RegisterReceptionistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterReceptionistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterReceptionistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
