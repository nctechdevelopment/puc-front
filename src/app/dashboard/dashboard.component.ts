import { Component } from '@angular/core';

import { AuthGuard } from '../auth/auth.guard';
import { LoginModel } from '../models/login.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  user: LoginModel;

  constructor(private authGuard: AuthGuard) {
    this.user = this.authGuard.getUser();
  }

}
