import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModalModule } from 'ngx-bootstrap/modal';
import { DataTablesModule } from 'angular-datatables';
import { NgxMaskModule } from 'ngx-mask';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '../shared/shared.module';

// Laboratories
import { ListLaboratoriesComponent } from './laboratories/list-laboratories/list-laboratories.component';
import { RegisterLaboratoriesComponent } from './laboratories/register-laboratories/register-laboratories.component';
import { UpdateLaboratoriesComponent } from './laboratories/update-laboratories/update-laboratories.component';
import { UpdateStatusLaboratoriesComponent } from './laboratories/update-status-laboratories/update-status-laboratories.component';
// Doctors
import { ListDoctorsComponent } from './doctors/list-doctors/list-doctors.component';
import { RegisterDoctorsComponent } from './doctors/register-doctors/register-doctors.component';
import { UpdateDoctorsComponent } from './doctors/update-doctors/update-doctors.component';
import { UpdateStatusDoctorsComponent } from './doctors/update-status-doctors/update-status-doctors.component';
// Receptionists
import { ListReceptionistsComponent } from './receptionists/list-receptionists/list-receptionists.component';
import { RegisterReceptionistsComponent } from './receptionists/register-receptionists/register-receptionists.component';
import { UpdateReceptionistsComponent } from './receptionists/update-receptionists/update-receptionists.component';
import { UpdateStatusReceptionistsComponent } from './receptionists/update-status-receptionists/update-status-receptionists.component';
// Medicaments
import { ListMedicamentsComponent } from './medicaments/list-medicaments/list-medicaments.component';
import { RegisterMedicamentsComponent } from './medicaments/register-medicaments/register-medicaments.component';
import { UpdateMedicamentsComponent } from './medicaments/update-medicaments/update-medicaments.component';
import { UpdateStatusMedicamentsComponent } from './medicaments/update-status-medicaments/update-status-medicaments.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ListLaboratoriesComponent,
    RegisterLaboratoriesComponent,
    UpdateLaboratoriesComponent,
    UpdateStatusLaboratoriesComponent,
    ListDoctorsComponent,
    RegisterDoctorsComponent,
    UpdateDoctorsComponent,
    UpdateStatusDoctorsComponent,
    ListReceptionistsComponent,
    RegisterReceptionistsComponent,
    UpdateReceptionistsComponent,
    UpdateStatusReceptionistsComponent,
    ListMedicamentsComponent,
    RegisterMedicamentsComponent,
    UpdateMedicamentsComponent,
    UpdateStatusMedicamentsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    DataTablesModule,
    NgxMaskModule.forRoot(),
    AngularMultiSelectModule,
  ],
})
export class DashboardModule { }
