import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { LaboratoriesService } from '../service/laboratories.service';

@Component({
  selector: 'app-update-status-laboratories',
  templateUrl: './update-status-laboratories.component.html',
  styleUrls: ['./update-status-laboratories.component.css'],
  providers: [LaboratoriesService]
})
export class UpdateStatusLaboratoriesComponent {

  @Output() emitUpdateStatusLaboratories: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  id: number;
  status: string;
  isModalShown = false;

  constructor(
    private laboratoriesService: LaboratoriesService,
    private toaster: ToasterService,
  ) { }

  public updateStatus() {
    this.laboratoriesService.updateStatus(this.id, (this.status === '1' ? '0' : '1')).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateStatusLaboratories.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number, status: string) {
    this.id = id;
    this.status = status;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
