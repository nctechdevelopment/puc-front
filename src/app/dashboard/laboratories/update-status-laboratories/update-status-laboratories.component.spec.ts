import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusLaboratoriesComponent } from './update-status-laboratories.component';

describe('UpdateStatusLaboratoriesComponent', () => {
  let component: UpdateStatusLaboratoriesComponent;
  let fixture: ComponentFixture<UpdateStatusLaboratoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateStatusLaboratoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusLaboratoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
