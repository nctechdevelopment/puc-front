import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { LaboratoryModel } from '../../../models/laboratory.model';

@Injectable({
  providedIn: 'root'
})
export class LaboratoriesService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/laboratory`;
  }

  searchAll(data: any): Observable<any> {
    return this.http.post(`${this.url}/search`, data).pipe(map((result: Array<LaboratoryModel>) => result));
  }

  register(fd: FormData): Observable<any> {
    return this.http.post(`${this.url}/register`, fd).pipe(map((result: any) => result));
  }

  update(fd: FormData): Observable<any> {
    return this.http.post(`${this.url}/update`, fd).pipe(map((result: any) => result));
  }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`).pipe(map((result: any) => result));
  }

  updateStatus(id: number, status: string): Observable<any> {
    return this.http.put(`${this.url}/${id}/status/${status}`, {}).pipe(map((result: any) => result));
  }

}
