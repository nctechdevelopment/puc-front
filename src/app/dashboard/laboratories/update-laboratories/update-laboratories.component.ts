import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { LaboratoryModel } from '../../../models/laboratory.model';
import { LaboratoriesService } from '../service/laboratories.service';
import { CepService } from '../../../shared/service/cep.service';

@Component({
  selector: 'app-update-laboratories',
  templateUrl: './update-laboratories.component.html',
  styleUrls: ['./update-laboratories.component.css'],
  providers: [LaboratoriesService]
})
export class UpdateLaboratoriesComponent implements OnInit {

  @Output() emitUpdateLaboratories: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  laboratory: LaboratoryModel;
  selectedFile: File;
  selectedFilename: string;
  isModalShown = false;

  constructor(
    private cepService: CepService,
    private laboratoriesService: LaboratoriesService,
    private toaster: ToasterService,
  ) {
    this.laboratory = new LaboratoryModel();
  }

  ngOnInit() {
  }

  public findById(id: number) {
    this.laboratoriesService.findById(id).subscribe((result: any) => {
      this.laboratory = result.laboratory;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.laboratory.imagemAntiga) { fd.append('imagemAntiga', this.laboratory.imagemAntiga); }
    fd.append('id', this.laboratory.id.toString());
    fd.append('nome', this.laboratory.nome);
    fd.append('razaoSocial', this.laboratory.razaoSocial);
    fd.append('cnpj', this.laboratory.cnpj);
    fd.append('cep', this.laboratory.cep);
    fd.append('estado', this.laboratory.estado);
    fd.append('cidade', this.laboratory.cidade);
    fd.append('bairro', this.laboratory.bairro);
    fd.append('logradouro', this.laboratory.logradouro);
    if (this.laboratory.complemento) { fd.append('complemento', this.laboratory.complemento); }
    fd.append('numero', this.laboratory.numero);
    fd.append('celular', this.laboratory.celular);
    if (this.laboratory.telefone) { fd.append('telefone', this.laboratory.telefone); }

    this.laboratoriesService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateLaboratories.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number) {
    this.findById(id);
    this.isModalShown = true;
  }

  public closeModal() {
    this.selectedFile = null;
    this.selectedFilename = '';
    this.laboratory = new LaboratoryModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public searchCep() {
    this.cepService.searchCep(this.laboratory.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.laboratory.complemento = result.complemento ? result.complemento : null;
      this.laboratory.logradouro = result.logradouro ? result.logradouro : null;
      this.laboratory.bairro = result.bairro ? result.bairro : null;
      this.laboratory.cidade = result.localidade ? result.localidade : null;
      this.laboratory.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

}
