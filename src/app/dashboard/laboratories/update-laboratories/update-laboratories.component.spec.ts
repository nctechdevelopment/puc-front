import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLaboratoriesComponent } from './update-laboratories.component';

describe('UpdateLaboratoriesComponent', () => {
  let component: UpdateLaboratoriesComponent;
  let fixture: ComponentFixture<UpdateLaboratoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateLaboratoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLaboratoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
