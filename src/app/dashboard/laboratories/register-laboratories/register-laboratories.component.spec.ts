import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterLaboratoriesComponent } from './register-laboratories.component';

describe('RegisterLaboratoriesComponent', () => {
  let component: RegisterLaboratoriesComponent;
  let fixture: ComponentFixture<RegisterLaboratoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterLaboratoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterLaboratoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
