import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { LaboratoryModel } from '../../../models/laboratory.model';
import { LaboratoriesService } from '../service/laboratories.service';
import { CepService } from '../../../shared/service/cep.service';

@Component({
  selector: 'app-register-laboratories',
  templateUrl: './register-laboratories.component.html',
  styleUrls: ['./register-laboratories.component.css'],
  providers: [LaboratoriesService]
})
export class RegisterLaboratoriesComponent {

  @Output() emitRegisterLaboratories: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  laboratory: LaboratoryModel;
  selectedFile: File;
  selectedFilename: string;
  differentPasswords: boolean;
  isModalShown = false;

  constructor(
    private cepService: CepService,
    private laboratoriesService: LaboratoriesService,
    private toaster: ToasterService,
  ) {
    this.laboratory = new LaboratoryModel();
  }

  public register() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    fd.append('nome', this.laboratory.nome);
    fd.append('razaoSocial', this.laboratory.razaoSocial);
    fd.append('cnpj', this.laboratory.cnpj);
    fd.append('cep', this.laboratory.cep);
    fd.append('estado', this.laboratory.estado);
    fd.append('cidade', this.laboratory.cidade);
    fd.append('bairro', this.laboratory.bairro);
    fd.append('logradouro', this.laboratory.logradouro);
    if (this.laboratory.complemento) { fd.append('complemento', this.laboratory.complemento); }
    fd.append('numero', this.laboratory.numero);
    fd.append('celular', this.laboratory.celular);
    if (this.laboratory.telefone) { fd.append('telefone', this.laboratory.telefone); }
    fd.append('email', this.laboratory.login.email);
    fd.append('login[email]', this.laboratory.login.email);
    fd.append('login[senha]', this.laboratory.login.senha);
    fd.append('login[nivelAcesso]', '2');

    this.laboratoriesService.register(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitRegisterLaboratories.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal() {
    this.isModalShown = true;
  }

  public closeModal() {
    this.selectedFile = null;
    this.selectedFilename = '';
    this.laboratory = new LaboratoryModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public searchCep() {
    this.cepService.searchCep(this.laboratory.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.laboratory.complemento = result.complemento ? result.complemento : null;
      this.laboratory.logradouro = result.logradouro ? result.logradouro : null;
      this.laboratory.bairro = result.bairro ? result.bairro : null;
      this.laboratory.cidade = result.localidade ? result.localidade : null;
      this.laboratory.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

  public validatePasswords() {
    this.differentPasswords = this.laboratory.login.senha !== this.laboratory.login.confirmarSenha;
  }

}
