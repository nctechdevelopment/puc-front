import { Component, AfterViewInit, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';

import { LaboratoryModel } from '../../../models/laboratory.model';
import { LaboratoriesService } from '../service/laboratories.service';
import { RegisterLaboratoriesComponent } from '../register-laboratories/register-laboratories.component';
import { UpdateLaboratoriesComponent } from '../update-laboratories/update-laboratories.component';
import { UpdateStatusLaboratoriesComponent } from '../update-status-laboratories/update-status-laboratories.component';
import { ChangePasswordComponent } from '../../../shared/change-password/change-password.component';

@Component({
  selector: 'app-list-laboratories',
  templateUrl: './list-laboratories.component.html',
  styleUrls: ['./list-laboratories.component.css'],
  providers: [LaboratoriesService]
})
export class ListLaboratoriesComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('modalRegisterLaboratories', { static: false }) public modalRegisterLaboratories: RegisterLaboratoriesComponent;
  @ViewChild('modalUpdateLaboratories', { static: false }) public modalUpdateLaboratories: UpdateLaboratoriesComponent;
  @ViewChild('modalUpdateStatusLaboratories', { static: false }) public modalUpdateStatusLaboratories: UpdateStatusLaboratoriesComponent;
  @ViewChild('modalChangePassword', { static: false }) public modalChangePassword: ChangePasswordComponent;

  page = 1;
  laboratories: Array<LaboratoryModel>;
  dtOptions: any = {};
  listenClickDownload: any;

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private laboratoriesService: LaboratoriesService,
    private toaster: ToasterService,
  ) {
    this.laboratories = new Array<LaboratoryModel>();
  }

  ngOnInit() {
    this.dtOptions = {
      autoWidth: false,
      responsive: true,
      serverSide: true,
      processing: true,
      searchDelay: 1000,
      ordering: false,
      pageLength: 25,
      pagingType: 'full_numbers',
      table: '#table-laboratories',
      dom: 'Bfrtip',
      buttons: [
        'pageLength',
        'colvis',
      ],
      columns: [
        {
          name: 'Nome fantasia',
          data: 'nome'
        },
        {
          name: 'Razão social',
          data: 'razao_social'
        },
        {
          name: 'CNPJ',
          data: 'cnpj'
        },
        {
          name: 'Celular',
          data: 'celular'
        },
        {
          name: 'Telefone',
          data: 'telefone'
        },
        {
          name: 'E-mail',
          data: 'email'
        },
        {
          name: 'Endereço completo',
          render: (data, type, row) => {
            return `
              ${row.logradouro}, ${row.numero}, ${row.bairro} -
              ${row.cidade}, ${row.estado}, ${row.cep}
            ` + (row.complemento ? `(${row.complemento})` : '');
          }
        },
        {
          name: 'Ações',
          data: 'id',
          render: (data, type, row) => {
            return `
              <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-sm" data-edit-laboratory-id="${data}">
                  <i class="fas fa-edit" data-edit-laboratory-id="${data}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-change-password-login-id="${row.login_id}">
                  <i class="fas fa-key" data-change-password-login-id="${row.login_id}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-update-status-laboratory-id="${data}">
                  ${
                    row.status === '1'
                    ? `<i class="fas fa-check-circle" data-update-status-laboratory-id="${data}"
                      data-update-status-laboratory-status="${row.status}"></i>`
                    : `<i class="fas fa-times-circle" data-update-status-laboratory-id="${data}"
                      data-update-status-laboratory-status="${row.status}"></i>`
                  }
                </button>
              </div>
            `;
          },
        },
      ],
      columnDefs: [
        {
          targets: [1, 6],
          visible: false,
        },
      ],
      language: {
        processing: 'Procesando...',
        search: 'Pesquisar:',
        lengthMenu: '_MENU_',
        info: 'Mostrando _START_ até _END_ de _TOTAL_ registros',
        infoEmpty: 'Mostrando 0 até 0 de 0 registros',
        infoFiltered: '(Filtrados de _MAX_ registros)',
        infoPostFix: '',
        sInfoThousands: '.',
        loadingRecords: 'Carregando...',
        zeroRecords: 'Nenhum registro encontrado',
        emptyTable: 'Nenhum registro encontrado',
        paginate: {
          next: 'Próximo',
          previous: 'Anterior',
          first: 'Primeiro',
          last: 'Último'
        },
        aria: {
          sortAscending: ': Ordenar colunas de forma ascendente',
          sortDescending: ': Ordenar colunas de forma descendente'
        },
        buttons: {
          colvis: 'Colunas visíveis',
          pageLength: {
            _: 'Mostrar %d resultados',
          }
        },
      },
      fnServerParams: () => {
        const table = $('#table-laboratories').DataTable();

        table.on('page.dt', () => {
          const info = table.page.info();
          this.page = info.page + 1;
        });

        table.on('search.dt', () => this.page = 1);
      },
    };

    this.searchAll();
  }

  ngOnDestroy() {
    this.listenClickDownload();
    this.renderer.destroy();
  }

  public searchAll() {
    this.dtOptions.ajax = (dataTablesParameters: any, callback) => {
      this.laboratoriesService.searchAll({
        page: this.page,
        search: dataTablesParameters.search.value,
        length: dataTablesParameters.length,
      }).subscribe((result: any) => {
        callback({
          recordsTotal: result.recordsTotal,
          recordsFiltered: result.recordsFiltered,
          data: result.laboratories
        });
      }, (error) => {
        console.log(error);
        this.toaster.pop('error', 'Ops!', error.error.message);
      });
    };
  }

  public dtSearch() {
    this.page = 1;

    $('#table-laboratories').DataTable().ajax.reload();
  }

  public ngAfterViewInit(): void {
    this.listenClickDownload = this.renderer.listen('document', 'click', (event) => {
      if (event.target.hasAttribute('data-edit-laboratory-id')) {
        this.openModalUpdateLaboratories(event.target.getAttribute('data-edit-laboratory-id'));
      } else if (event.target.hasAttribute('data-update-status-laboratory-id')) {
        this.openModalUpdateStatusLaboratories(
          event.target.getAttribute('data-update-status-laboratory-id'),
          event.target.getAttribute('data-update-status-laboratory-status')
        );
      } else if (event.target.hasAttribute('data-change-password-login-id')) {
        this.openModalChangePassword(event.target.getAttribute('data-change-password-login-id'));
      }
    });
  }

  public subscriberLaboratories() {
    this.dtSearch();
  }

  public openModalRegisterLaboratories() {
    this.modalRegisterLaboratories.openModal();
  }

  public openModalUpdateLaboratories(id: number) {
    this.modalUpdateLaboratories.openModal(id);
  }

  public openModalUpdateStatusLaboratories(id: number, status: string) {
    this.modalUpdateStatusLaboratories.openModal(id, status);
  }

  public openModalChangePassword(loginId: number) {
    this.modalChangePassword.openModal(loginId);
  }

}
