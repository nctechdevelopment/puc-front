import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { DoctorModel } from '../../../models/doctor.model';
import { DoctorsService } from '../service/doctors.service';
import { CepService } from '../../../shared/service/cep.service';
import { Specialties } from '../../../shared/datas/specialties';
import { Settings } from '../../../shared/settings/settings';

@Component({
  selector: 'app-update-doctors',
  templateUrl: './update-doctors.component.html',
  styleUrls: ['./update-doctors.component.css'],
  providers: [DoctorsService]
})
export class UpdateDoctorsComponent implements OnInit {

  @Output() emitUpdateDoctors: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  doctor: DoctorModel;
  selectedFile: File;
  selectedFilename: string;
  isModalShown = false;

  dropdownSettings = {};
  specialties: Array<{ id: number, info: string }>;
  selectedSpecialties: Array<{ id: number, info: string }>;

  constructor(
    private cepService: CepService,
    private doctorsService: DoctorsService,
    private toaster: ToasterService,
  ) {
    this.doctor = new DoctorModel();
    this.specialties = new Array<{ id: number, info: string }>();
    this.selectedSpecialties = new Array<{ id: number, info: string }>();
  }

  ngOnInit() {
    this.specialties = Specialties.specialtyTypes;
    this.dropdownSettings = Settings.dropdown;
  }

  public findById(id: number) {
    this.doctorsService.findById(id).subscribe((result: any) => {
      this.doctor = result.doctor;
      this.doctor.dataNascimento = moment(this.doctor.dataNascimento).format('DD/MM/YYYY');
      this.selectedSpecialties = JSON.parse(result.doctor.especialidades);
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.doctor.imagemAntiga) { fd.append('imagemAntiga', this.doctor.imagemAntiga); }
    fd.append('id', this.doctor.id.toString());
    fd.append('nome', this.doctor.nome);
    fd.append('dataNascimento', this.doctor.dataNascimento);
    fd.append('especialidades', JSON.stringify(this.selectedSpecialties));
    fd.append('cpf', this.doctor.cpf);
    fd.append('rg', this.doctor.rg);
    fd.append('crm', this.doctor.crm);
    fd.append('cep', this.doctor.cep);
    fd.append('estado', this.doctor.estado);
    fd.append('cidade', this.doctor.cidade);
    fd.append('bairro', this.doctor.bairro);
    fd.append('logradouro', this.doctor.logradouro);
    if (this.doctor.complemento) { fd.append('complemento', this.doctor.complemento); }
    fd.append('numero', this.doctor.numero);
    fd.append('celular', this.doctor.celular);
    if (this.doctor.telefone) { fd.append('telefone', this.doctor.telefone); }

    this.doctorsService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateDoctors.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number) {
    this.findById(id);
    this.isModalShown = true;
  }

  public closeModal() {
    this.selectedFile = null;
    this.selectedFilename = '';
    this.doctor = new DoctorModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public searchCep() {
    this.cepService.searchCep(this.doctor.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.doctor.complemento = result.complemento ? result.complemento : null;
      this.doctor.logradouro = result.logradouro ? result.logradouro : null;
      this.doctor.bairro = result.bairro ? result.bairro : null;
      this.doctor.cidade = result.localidade ? result.localidade : null;
      this.doctor.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

  public onDeSelectAll() {
    this.selectedSpecialties = [];
  }

}
