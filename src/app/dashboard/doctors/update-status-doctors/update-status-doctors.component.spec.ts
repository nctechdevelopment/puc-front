import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusDoctorsComponent } from './update-status-doctors.component';

describe('UpdateStatusDoctorsComponent', () => {
  let component: UpdateStatusDoctorsComponent;
  let fixture: ComponentFixture<UpdateStatusDoctorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateStatusDoctorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
