import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { DoctorsService } from '../service/doctors.service';

@Component({
  selector: 'app-update-status-doctors',
  templateUrl: './update-status-doctors.component.html',
  styleUrls: ['./update-status-doctors.component.css'],
  providers: [DoctorsService]
})
export class UpdateStatusDoctorsComponent {

  @Output() emitUpdateStatusDoctors: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  id: number;
  status: string;
  isModalShown = false;

  constructor(
    private doctorsService: DoctorsService,
    private toaster: ToasterService,
  ) { }

  public updateStatus() {
    this.doctorsService.updateStatus(this.id, (this.status === '1' ? '0' : '1')).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateStatusDoctors.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number, status: string) {
    this.id = id;
    this.status = status;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
