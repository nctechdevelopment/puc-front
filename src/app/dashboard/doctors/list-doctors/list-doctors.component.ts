import { Component, AfterViewInit, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { DoctorModel } from '../../../models/doctor.model';
import { DoctorsService } from '../service/doctors.service';
import { RegisterDoctorsComponent } from '../register-doctors/register-doctors.component';
import { UpdateDoctorsComponent } from '../update-doctors/update-doctors.component';
import { UpdateStatusDoctorsComponent } from '../update-status-doctors/update-status-doctors.component';
import { ChangePasswordComponent } from '../../../shared/change-password/change-password.component';

@Component({
  selector: 'app-list-doctors',
  templateUrl: './list-doctors.component.html',
  styleUrls: ['./list-doctors.component.css'],
  providers: [DoctorsService]
})
export class ListDoctorsComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('modalRegisterDoctors', { static: false }) public modalRegisterDoctors: RegisterDoctorsComponent;
  @ViewChild('modalUpdateDoctors', { static: false }) public modalUpdateDoctors: UpdateDoctorsComponent;
  @ViewChild('modalUpdateStatusDoctors', { static: false }) public modalUpdateStatusDoctors: UpdateStatusDoctorsComponent;
  @ViewChild('modalChangePassword', { static: false }) public modalChangePassword: ChangePasswordComponent;

  page = 1;
  doctors: Array<DoctorModel>;
  dtOptions: any = {};
  listenClickDownload: any;

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private doctorsService: DoctorsService,
    private toaster: ToasterService,
  ) {
    this.doctors = new Array<DoctorModel>();
  }

  ngOnInit() {
    this.dtOptions = {
      autoWidth: false,
      responsive: true,
      serverSide: true,
      processing: true,
      searchDelay: 1000,
      ordering: false,
      pageLength: 25,
      pagingType: 'full_numbers',
      table: '#table-doctors',
      dom: 'Bfrtip',
      buttons: [
        'pageLength',
        'colvis',
      ],
      columns: [
        {
          name: 'Nome',
          data: 'nome'
        },
        {
          name: 'Data de nascimento',
          data: 'data_nascimento',
          render: (data) => {
            if (moment(data).isValid()) {
              return moment(data).format('DD/MM/YYYY');
            } else { return ''; }
          }
        },
        {
          name: 'CPF',
          data: 'cpf'
        },
        {
          name: 'RG',
          data: 'rg'
        },
        {
          name: 'CRM',
          data: 'crm'
        },
        {
          name: 'Celular',
          data: 'celular'
        },
        {
          name: 'Telefone',
          data: 'telefone'
        },
        {
          name: 'E-mail',
          data: 'email'
        },
        {
          name: 'Endereço completo',
          render: (data, type, row) => {
            return `
              ${row.logradouro}, ${row.numero}, ${row.bairro} -
              ${row.cidade}, ${row.estado}, ${row.cep}
              ` + (row.complemento ? `(${row.complemento})` : '');
          }
        },
        {
          name: 'Especialidades',
          data: 'especialidades',
          render: (data) => {
            const specialties = JSON.parse(data);
            return specialties.map(elem => ' ' + elem.info);
          },
        },
        {
          name: 'Ações',
          data: 'id',
          render: (data, type, row) => {
            return `
              <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-sm" data-edit-doctor-id="${data}">
                  <i class="fas fa-edit" data-edit-doctor-id="${data}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-change-password-login-id="${row.login_id}">
                  <i class="fas fa-key" data-change-password-login-id="${row.login_id}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-update-status-doctor-id="${data}">
                  ${
                    row.status === '1'
                      ? `<i class="fas fa-check-circle" data-update-status-doctor-id="${data}"
                          data-update-status-doctor-status="${row.status}"></i>`
                      : `<i class="fas fa-times-circle" data-update-status-doctor-id="${data}"
                          data-update-status-doctor-status="${row.status}"></i>`
                  }
                </button>
              </div>
            `;
          },
        },
      ],
      columnDefs: [
        {
          targets: [1, 3, 4, 8],
          visible: false,
        },
        {
          responsivePriority: -1,
          targets: 9
        },
      ],
      language: {
        processing: 'Procesando...',
        search: 'Pesquisar:',
        lengthMenu: '_MENU_',
        info: 'Mostrando _START_ até _END_ de _TOTAL_ registros',
        infoEmpty: 'Mostrando 0 até 0 de 0 registros',
        infoFiltered: '(Filtrados de _MAX_ registros)',
        infoPostFix: '',
        sInfoThousands: '.',
        loadingRecords: 'Carregando...',
        zeroRecords: 'Nenhum registro encontrado',
        emptyTable: 'Nenhum registro encontrado',
        paginate: {
          next: 'Próximo',
          previous: 'Anterior',
          first: 'Primeiro',
          last: 'Último'
        },
        aria: {
          sortAscending: ': Ordenar colunas de forma ascendente',
          sortDescending: ': Ordenar colunas de forma descendente'
        },
        buttons: {
          colvis: 'Colunas visíveis',
          pageLength: {
            _: 'Mostrar %d resultados',
          }
        },
      },
      fnServerParams: () => {
        const table = $('#table-doctors').DataTable();

        table.on('page.dt', () => {
          const info = table.page.info();
          this.page = info.page + 1;
        });

        table.on('search.dt', () => this.page = 1);
      },
    };

    this.searchAll();
  }

  ngOnDestroy() {
    this.listenClickDownload();
    this.renderer.destroy();
  }

  public searchAll() {
    this.dtOptions.ajax = (dataTablesParameters: any, callback) => {
      this.doctorsService.searchAll({
        page: this.page,
        search: dataTablesParameters.search.value,
        length: dataTablesParameters.length,
      }).subscribe((result: any) => {
        callback({
          recordsTotal: result.recordsTotal,
          recordsFiltered: result.recordsFiltered,
          data: result.doctors
        });
      }, (error) => {
        console.log(error);
        this.toaster.pop('error', 'Ops!', error.error.message);
      });
    };
  }

  public dtSearch() {
    this.page = 1;

    $('#table-doctors').DataTable().ajax.reload();
  }

  public ngAfterViewInit(): void {
    this.listenClickDownload = this.renderer.listen('document', 'click', (event) => {
      if (event.target.hasAttribute('data-edit-doctor-id')) {
        this.openModalUpdateDoctors(event.target.getAttribute('data-edit-doctor-id'));
      } else if (event.target.hasAttribute('data-update-status-doctor-id')) {
        this.openModalUpdateStatusDoctors(
          event.target.getAttribute('data-update-status-doctor-id'),
          event.target.getAttribute('data-update-status-doctor-status')
        );
      } else if (event.target.hasAttribute('data-change-password-login-id')) {
        this.openModalChangePassword(event.target.getAttribute('data-change-password-login-id'));
      }
    });
  }

  public subscriberDoctors() {
    this.dtSearch();
  }

  public openModalRegisterDoctors() {
    this.modalRegisterDoctors.openModal();
  }

  public openModalUpdateDoctors(id: number) {
    this.modalUpdateDoctors.openModal(id);
  }

  public openModalUpdateStatusDoctors(id: number, status: string) {
    this.modalUpdateStatusDoctors.openModal(id, status);
  }

  public openModalChangePassword(loginId: number) {
    this.modalChangePassword.openModal(loginId);
  }

}
