import { MedicalRecordModel } from './medical-record.model';

export class CertificateModel {
  public id: number;
  public periodo: string;
  public prontuario: MedicalRecordModel = new MedicalRecordModel();
}
