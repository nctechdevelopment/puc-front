import { MedicalRecordModel } from './medical-record.model';

export class MedicamentModel {
  public id: number;
  public nomeGenerico: string;
  public nomeFabrica: string;
  public nomeFabricante: string;
  public status ?: string;
}

export class PrescriptionModel {
  public id: number;
  public posologia: string;
  public medicamento: MedicamentModel = new MedicamentModel();
  public prontuario: MedicalRecordModel = new MedicalRecordModel();
}
