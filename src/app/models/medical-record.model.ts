import { DoctorModel } from './doctor.model';
import { PatientModel } from './patient.model';
import { ScheduleModel } from './schedule.model';

export class MedicalRecordModel {
  public id: number;
  public peso: number;
  public altura: number;
  public imc: number;
  public pressArter: string;
  public freqCard: string;
  public freqResp: string;
  public temperatura: string;
  public queixaPrincipal: string;
  public histDoencaAtual: string;
  public interrSintom: string;
  public antecPessFamil: string;
  public medico: DoctorModel = new DoctorModel();
  public paciente: PatientModel = new PatientModel();
  public agendamento: ScheduleModel = new ScheduleModel();
}
