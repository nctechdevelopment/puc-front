import { DoctorModel } from './doctor.model';
import { PatientModel } from './patient.model';

export class ScheduleModel {
  public id: number;
  public titulo: string;
  public tipo = '';
  public status: string;
  public dataInicio: string;
  public dataFim: string;
  public dataAgendamento: string;
  public medico: DoctorModel = new DoctorModel();
  public paciente: PatientModel = new PatientModel();
}
