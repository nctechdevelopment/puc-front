import { LoginModel } from './login.model';

export class DoctorModel {
  public id: number;
  public nome: string;
  public imagem: string;
  public imagemAntiga: string;
  public dataNascimento: string;
  public especialidades = new Array<{ id: number, info: string }>();
  public cpf: string;
  public rg: string;
  public crm: string;
  public cep: string;
  public estado: string;
  public cidade: string;
  public bairro: string;
  public logradouro: string;
  public complemento: string;
  public numero: string;
  public celular: string;
  public telefone: string;
  public email: string;
  public status: string;
  public login: LoginModel = new LoginModel();
}
