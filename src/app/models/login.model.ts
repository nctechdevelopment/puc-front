export class LoginModel {
  public id: number;
  public login: string;
  public loginId: number;
  public nome: string;
  public imagem: string;
  public email: string;
  public senha: string;
  public novaSenha: string;
  public confirmarSenha: string;
  public nivelAcesso: string;
}
