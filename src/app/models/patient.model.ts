export class PatientModel {
  public id: number;
  public nome: string;
  public nomeMae: string;
  public nomeResponsavel ?: string;
  public sexo = '';
  public tipoSanguineo = '';
  public imagem ?: string;
  public imagemAntiga ?: string;
  public dataNascimento: string;
  public idade ?: string;
  public cpf: string;
  public cpfResponsavel ?: string;
  public rg: string;
  public cns: string;
  public cep: string;
  public estado: string;
  public cidade: string;
  public bairro: string;
  public logradouro: string;
  public complemento: string;
  public numero: string;
  public celular: string;
  public telefone: string;
  public email: string;
  public status ?: string;
  // Others
  public titulo ?: string;
  public info ?: string;
}
