import { MedicalRecordModel } from './medical-record.model';

export class ExamModel {
  public id: number;
  public login ?: string;
  public senha ?: string;
  public resultado ?: string;
  public statusEntrega ?: string;
  public dataEntrega ?: string;
  public dataSolicitacao ?: string;
  public prontuario: MedicalRecordModel = new MedicalRecordModel();
}

export class ExamTypeModel {
  public id: number;
  public nome: string;
}

export class ExamExamTypeModel {
  public id: number;
  public exame: ExamModel = new ExamModel();
  public tipoExame: Array<ExamTypeModel> = new Array<ExamTypeModel>();
}
