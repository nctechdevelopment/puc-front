import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { LoginModel } from '../../models/login.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/access`;
  }

  changePassword(loginId: number, user: LoginModel): Observable<any> {
    return this.http.put(`${this.url}/change-password/${loginId}`, user).pipe(map((result: any) => result));
  }

}
