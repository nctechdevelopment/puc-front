import { Component, ViewChild } from '@angular/core';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { LoginModel } from '../../models/login.model';
import { ChangePasswordService } from './change-password.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  providers: [ChangePasswordService]
})
export class ChangePasswordComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  loginId: number;
  user: LoginModel;
  differentPasswords: boolean;
  isModalShown = false;

  constructor(
    private changePasswordService: ChangePasswordService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
  }

  public changePassword() {
    this.changePasswordService.changePassword(this.loginId, this.user).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.closeModal();
    }, (error) => {
      console.log(error);
      const toast: Toast = {
        type: 'error',
        title: 'Ops!',
        body: error.error.message,
        bodyOutputType: BodyOutputType.TrustedHtml
      };
      this.toaster.pop(toast);
    });
  }

  public openModal(loginId: number) {
    this.loginId = loginId;
    this.isModalShown = true;
  }

  public closeModal() {
    this.user = new LoginModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public validatePasswords() {
    this.differentPasswords = this.user.novaSenha !== this.user.confirmarSenha;
  }

}
