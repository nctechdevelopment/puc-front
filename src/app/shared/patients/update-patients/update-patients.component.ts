import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { PatientModel } from '../../../models/patient.model';
import { PatientsService } from '../service/patients.service';
import { CepService } from '../../../shared/service/cep.service';

@Component({
  selector: 'app-update-patients',
  templateUrl: './update-patients.component.html',
  styleUrls: ['./update-patients.component.css'],
  providers: [PatientsService]
})
export class UpdatePatientsComponent implements OnInit {

  @Output() emitUpdatePatients: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  patient: PatientModel;
  selectedFile: File;
  selectedFilename: string;
  isModalShown = false;
  responsible = false;

  constructor(
    private cepService: CepService,
    private patientsService: PatientsService,
    private toaster: ToasterService,
  ) {
    this.patient = new PatientModel();
  }

  ngOnInit() {
  }

  public findById(id: number) {
    this.patientsService.findById(id).subscribe((result: any) => {
      this.patient = result.patient;
      this.patient.dataNascimento = moment(result.patient.dataNascimento).format('DD/MM/YYYY');
      this.responsible = this.calculeAge(result.patient.dataNascimento);
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.patient.imagemAntiga) { fd.append('imagemAntiga', this.patient.imagemAntiga); }
    fd.append('id', this.patient.id.toString());
    fd.append('nome', this.patient.nome);
    fd.append('nomeMae', this.patient.nomeMae);
    fd.append('sexo', this.patient.sexo);
    if (this.patient.tipoSanguineo) { fd.append('tipoSanguineo', this.patient.tipoSanguineo); }
    fd.append('dataNascimento', this.patient.dataNascimento);
    fd.append('cpf', this.patient.cpf);
    fd.append('rg', this.patient.rg);
    fd.append('cns', this.patient.cns);
    fd.append('cep', this.patient.cep);
    fd.append('estado', this.patient.estado);
    fd.append('cidade', this.patient.cidade);
    fd.append('bairro', this.patient.bairro);
    fd.append('logradouro', this.patient.logradouro);
    if (this.patient.complemento) { fd.append('complemento', this.patient.complemento); }
    fd.append('numero', this.patient.numero);
    fd.append('celular', this.patient.celular);
    if (this.patient.telefone) { fd.append('telefone', this.patient.telefone); }
    if (this.patient.email) { fd.append('email', this.patient.email); }
    if (this.patient.nomeResponsavel) { fd.append('nomeResponsavel', this.patient.nomeResponsavel); }
    if (this.patient.cpfResponsavel) { fd.append('cpfResponsavel', this.patient.cpfResponsavel); }

    this.patientsService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdatePatients.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number) {
    this.findById(id);
    this.isModalShown = true;
  }

  public closeModal() {
    this.selectedFile = null;
    this.selectedFilename = '';
    this.patient = new PatientModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public searchCep() {
    this.cepService.searchCep(this.patient.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.patient.complemento = result.complemento ? result.complemento : null;
      this.patient.logradouro = result.logradouro ? result.logradouro : null;
      this.patient.bairro = result.bairro ? result.bairro : null;
      this.patient.cidade = result.localidade ? result.localidade : null;
      this.patient.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

  public validateBirth(event: any) {
    if (event.target.value.length === 10) {
      const format = event.target.value.split('/').reverse().join('-');
      this.responsible = this.calculeAge(format);
    } else { this.responsible = false; }
  }

  public calculeAge(birthDate: string) {
    const today = moment();
    const birth = moment(birthDate);
    const age = today.diff(birth, 'years');
    return age < 18 ? true : false;
  }

}
