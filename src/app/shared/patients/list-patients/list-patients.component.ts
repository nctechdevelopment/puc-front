import { Component, AfterViewInit, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { LoginModel } from '../../../models/login.model';
import { AuthGuard } from '../../../auth/auth.guard';
import { PatientModel } from '../../../models/patient.model';
import { PatientsService } from '../service/patients.service';
import { RegisterPatientsComponent } from '../register-patients/register-patients.component';
import { UpdatePatientsComponent } from '../update-patients/update-patients.component';
import { UpdateStatusPatientsComponent } from '../update-status-patients/update-status-patients.component';
import { ListHistoricsComponent } from '../list-historics/list-historics.component';
import { UtilsService } from '../../../shared/service/utils.service';

@Component({
  selector: 'app-list-patients',
  templateUrl: './list-patients.component.html',
  styleUrls: ['./list-patients.component.css'],
  providers: [PatientsService]
})
export class ListPatientsComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('modalRegisterPatients', { static: false }) public modalRegisterPatients: RegisterPatientsComponent;
  @ViewChild('modalUpdatePatients', { static: false }) public modalUpdatePatients: UpdatePatientsComponent;
  @ViewChild('modalUpdateStatusPatients', { static: false }) public modalUpdateStatusPatients: UpdateStatusPatientsComponent;
  @ViewChild('modalListMedicalReportsPatients', { static: false }) public modalListMedicalReportsPatients: ListHistoricsComponent;

  page = 1;
  user: LoginModel;
  patients: Array<PatientModel>;
  dtOptions: any = {};
  listenClickDownload: any;

  constructor(
    private authGuard: AuthGuard,
    private renderer: Renderer2,
    private utilsService: UtilsService,
    private patientsService: PatientsService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
    this.user = this.authGuard.getUser();

    this.patients = new Array<PatientModel>();
  }

  ngOnInit() {
    this.dtOptions = {
      autoWidth: false,
      responsive: true,
      serverSide: true,
      processing: true,
      searchDelay: 1000,
      ordering: false,
      pageLength: 25,
      pagingType: 'full_numbers',
      table: '#table-patients',
      dom: 'Bfrtip',
      buttons: [
        'pageLength',
        'colvis',
      ],
      columns: [
        {
          name: 'Nome',
          data: 'nome'
        },
        {
          name: 'Nome da mãe',
          data: 'nome_mae'
        },
        {
          name: 'Data de nascimento',
          data: 'data_nascimento',
          render: (data) => {
            if (moment(data).isValid()) {
              return moment(data).format('DD/MM/YYYY');
            } else { return ''; }
          }
        },
        {
          name: 'Idade',
          render: (data, type, row) => {
            if (moment(row.data_nascimento).isValid()) {
              return this.utilsService.calculateAge(
                moment(row.data_nascimento).format('DD/MM/YYYY'),
                moment().format('DD/MM/YYYY')
              );
            } else { return ''; }
          }
        },
        {
          name: 'CPF',
          data: 'cpf'
        },
        {
          name: 'RG',
          data: 'rg'
        },
        {
          name: 'Celular',
          data: 'celular'
        },
        {
          name: 'Telefone',
          data: 'telefone'
        },
        {
          name: 'E-mail',
          data: 'email'
        },
        {
          name: 'Endereço completo',
          render: (data, type, row) => {
            return `
              ${row.logradouro}, ${row.numero}, ${row.bairro} -
              ${row.cidade}, ${row.estado}, ${row.cep}
            ` + (row.complemento ? `(${row.complemento})` : '');
          }
        },
        {
          name: 'Nome do responsável',
          data: 'nome_responsavel'
        },
        {
          name: 'CPF do responsável',
          data: 'cpf_responsavel'
        },
        {
          name: 'Ações',
          data: 'id',
          render: (data, type, row) => {
            return `
              <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-sm" data-edit-patient-id="${data}">
                  <i class="fas fa-edit" data-edit-patient-id="${data}"></i>
                </button>
                <button type="button" class="btn btn-secondary btn-sm" data-update-status-patient-id="${data}">
                  ${
              row.status === '1'
                ? `<i class="fas fa-check-circle" data-update-status-patient-id="${data}"
                          data-update-status-patient-status="${row.status}"></i>`
                : `<i class="fas fa-times-circle" data-update-status-patient-id="${data}"
                          data-update-status-patient-status="${row.status}"></i>`
              }
                </button>
                ${
              this.user.nivelAcesso === '3'
                ? `<button type= "button" class="btn btn-secondary btn-sm" data-medical-record-patient-id="${data}">
                        <i class="fas fa-paste" data-medical-record-patient-id="${data}"></i>
                      </button>`
                : ''
              }
              </div>
            `;
          },
        },
      ],
      columnDefs: [
        {
          targets: [1, 2, 5, 9, 10, 11],
          visible: false,
        },
      ],
      language: {
        processing: 'Procesando...',
        search: 'Pesquisar:',
        lengthMenu: '_MENU_',
        info: 'Mostrando _START_ até _END_ de _TOTAL_ registros',
        infoEmpty: 'Mostrando 0 até 0 de 0 registros',
        infoFiltered: '(Filtrados de _MAX_ registros)',
        infoPostFix: '',
        sInfoThousands: '.',
        loadingRecords: 'Carregando...',
        zeroRecords: 'Nenhum registro encontrado',
        emptyTable: 'Nenhum registro encontrado',
        paginate: {
          next: 'Próximo',
          previous: 'Anterior',
          first: 'Primeiro',
          last: 'Último'
        },
        aria: {
          sortAscending: ': Ordenar colunas de forma ascendente',
          sortDescending: ': Ordenar colunas de forma descendente'
        },
        buttons: {
          colvis: 'Colunas visíveis',
          pageLength: {
            _: 'Mostrar %d resultados',
          }
        },
      },
      fnServerParams: () => {
        const table = $('#table-patients').DataTable();

        table.on('page.dt', () => {
          const info = table.page.info();
          this.page = info.page + 1;
        });

        table.on('search.dt', () => this.page = 1);
      },
    };

    this.searchAll();
  }

  ngOnDestroy() {
    this.listenClickDownload();
    this.renderer.destroy();
  }

  public searchAll() {
    this.dtOptions.ajax = (dataTablesParameters: any, callback) => {
      this.patientsService.searchAll({
        page: this.page,
        search: dataTablesParameters.search.value,
        length: dataTablesParameters.length,
      }).subscribe((result: any) => {
        callback({
          recordsTotal: result.recordsTotal,
          recordsFiltered: result.recordsFiltered,
          data: result.patients
        });
      }, (error) => {
        console.log(error);
        this.toaster.pop('error', 'Ops!', error.error.message);
      });
    };
  }

  public dtSearch() {
    this.page = 1;

    $('#table-patients').DataTable().ajax.reload();
  }

  public ngAfterViewInit(): void {
    this.listenClickDownload = this.renderer.listen('document', 'click', (event) => {
      if (event.target.hasAttribute('data-edit-patient-id')) {
        this.openModalUpdatePatients(event.target.getAttribute('data-edit-patient-id'));
      } else if (event.target.hasAttribute('data-update-status-patient-id')) {
        this.openModalUpdateStatusPatients(
          event.target.getAttribute('data-update-status-patient-id'),
          event.target.getAttribute('data-update-status-patient-status')
        );
      } else if (event.target.hasAttribute('data-medical-record-patient-id')) {
        this.openModalListMedicalReportsPatients(event.target.getAttribute('data-medical-record-patient-id'));
      }
    });
  }

  public subscriberPatients() {
    this.dtSearch();
  }

  public openModalRegisterPatients() {
    this.modalRegisterPatients.openModal();
  }

  public openModalUpdatePatients(id: number) {
    this.modalUpdatePatients.openModal(id);
  }

  public openModalUpdateStatusPatients(id: number, status: string) {
    this.modalUpdateStatusPatients.openModal(id, status);
  }

  public openModalListMedicalReportsPatients(id: number) {
    this.modalListMedicalReportsPatients.openModal(id);
  }

}
