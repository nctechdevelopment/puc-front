import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStatusPatientsComponent } from './update-status-patients.component';

describe('UpdateStatusPatientsComponent', () => {
  let component: UpdateStatusPatientsComponent;
  let fixture: ComponentFixture<UpdateStatusPatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateStatusPatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusPatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
