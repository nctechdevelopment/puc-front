import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { PatientsService } from '../service/patients.service';

@Component({
  selector: 'app-update-status-patients',
  templateUrl: './update-status-patients.component.html',
  styleUrls: ['./update-status-patients.component.css'],
  providers: [PatientsService]
})
export class UpdateStatusPatientsComponent {

  @Output() emitUpdateStatusPatients: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  id: number;
  status: string;
  isModalShown = false;

  constructor(
    private patientsService: PatientsService,
    private toaster: ToasterService,
  ) { }

  public updateStatus() {
    this.patientsService.updateStatus(this.id, (this.status === '1' ? '0' : '1')).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitUpdateStatusPatients.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(id: number, status: string) {
    this.id = id;
    this.status = status;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
