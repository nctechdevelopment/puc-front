import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { MedicalRecordModel } from '../../../models/medical-record.model';
import { HistoricService } from '../../../dashboard-doctor/historic/service/historic.service';

@Component({
  selector: 'app-list-historics',
  templateUrl: './list-historics.component.html',
  styleUrls: ['./list-historics.component.css'],
  providers: [HistoricService]
})
export class ListHistoricsComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  page = 0;
  finished = false;
  isModalShown = false;
  patientId: number;
  records: Array<MedicalRecordModel>;

  constructor(
    private historicService: HistoricService,
    private toaster: ToasterService,
  ) {
    this.records = new Array<any>();
  }

  public findMedicalRecordHistoricByPatientId() {
    this.page++;

    this.historicService.findMedicalRecordHistoricByPatientId(this.patientId, this.page).subscribe((result: any) => {
      this.records = result.records;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(patientId: number) {
    this.patientId = patientId;
    this.isModalShown = true;
    this.findMedicalRecordHistoricByPatientId();
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
