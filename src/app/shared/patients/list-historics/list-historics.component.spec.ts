import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHistoricsComponent } from './list-historics.component';

describe('ListHistoricsComponent', () => {
  let component: ListHistoricsComponent;
  let fixture: ComponentFixture<ListHistoricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHistoricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHistoricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
