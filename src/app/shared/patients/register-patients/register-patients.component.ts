import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { PatientModel } from '../../../models/patient.model';
import { PatientsService } from '../service/patients.service';
import { CepService } from '../../../shared/service/cep.service';

@Component({
  selector: 'app-register-patients',
  templateUrl: './register-patients.component.html',
  styleUrls: ['./register-patients.component.css'],
  providers: [PatientsService]
})
export class RegisterPatientsComponent {

  @Output() emitRegisterPatients: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  patient: PatientModel;
  selectedFile: File;
  selectedFilename: string;
  differentPasswords: boolean;
  isModalShown = false;
  responsible = false;

  constructor(
    private cepService: CepService,
    private patientsService: PatientsService,
    private toaster: ToasterService,
  ) {
    this.patient = new PatientModel();
  }

  public register() {
    const fd = new FormData();
    fd.append('nome', this.patient.nome);
    fd.append('nomeMae', this.patient.nomeMae);
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.patient.sexo) { fd.append('sexo', this.patient.sexo); }
    if (this.patient.tipoSanguineo) { fd.append('tipoSanguineo', this.patient.tipoSanguineo); }
    if (this.patient.dataNascimento) { fd.append('dataNascimento', this.patient.dataNascimento); }
    if (this.patient.cpf) { fd.append('cpf', this.patient.cpf); }
    if (this.patient.rg) { fd.append('rg', this.patient.rg); }
    if (this.patient.cns) { fd.append('cns', this.patient.cns); }
    if (this.patient.cep) { fd.append('cep', this.patient.cep); }
    if (this.patient.estado) { fd.append('estado', this.patient.estado); }
    if (this.patient.cidade) { fd.append('cidade', this.patient.cidade); }
    if (this.patient.bairro) { fd.append('bairro', this.patient.bairro); }
    if (this.patient.logradouro) { fd.append('logradouro', this.patient.logradouro); }
    if (this.patient.complemento) { fd.append('complemento', this.patient.complemento); }
    if (this.patient.numero) { fd.append('numero', this.patient.numero); }
    if (this.patient.celular) { fd.append('celular', this.patient.celular); }
    if (this.patient.telefone) { fd.append('telefone', this.patient.telefone); }
    if (this.patient.email) { fd.append('email', this.patient.email); }
    if (this.patient.nomeResponsavel) { fd.append('nomeResponsavel', this.patient.nomeResponsavel); }
    if (this.patient.cpfResponsavel) { fd.append('cpfResponsavel', this.patient.cpfResponsavel); }

    this.patientsService.register(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.emitRegisterPatients.emit();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal() {
    this.isModalShown = true;
  }

  public closeModal() {
    this.selectedFile = null;
    this.selectedFilename = '';
    this.patient = new PatientModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public searchCep() {
    this.cepService.searchCep(this.patient.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.patient.complemento = result.complemento ? result.complemento : null;
      this.patient.logradouro = result.logradouro ? result.logradouro : null;
      this.patient.bairro = result.bairro ? result.bairro : null;
      this.patient.cidade = result.localidade ? result.localidade : null;
      this.patient.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

  public validateBirth(event: any) {
    if (event.target.value.length === 10) {
      const format = event.target.value.split('/').reverse().join('-');
      this.responsible = this.calculeAge(format);
    } else { this.responsible = false; }
  }

  public calculeAge(birthDate: string) {
    const today = moment();
    const birth = moment(birthDate);
    const age = today.diff(birth, 'years');
    return age < 18 ? true : false;
  }

}
