export class Specialties {
  public static specialtyTypes = [
    {
      id: 1,
      info: 'Alergia e Imunologia',
    },
    {
      id: 2,
      info: 'Anestesiologia',
    },
    {
      id: 3,
      info: 'Angiologia',
    },
    {
      id: 4,
      info: 'Cardiologia',
    },
    {
      id: 5,
      info: 'Cirurgia Cardiovascular',
    },
    {
      id: 6,
      info: 'Cirurgia da Mão',
    },
    {
      id: 9,
      info: 'Cirurgia de cabeça e pescoço',
    },
    {
      id: 10,
      info: 'Cirurgia do Aparelho Digestivo',
    },
    {
      id: 11,
      info: 'Cirurgia Geral',
    },
    {
      id: 12,
      info: 'Cirurgia Pediátrica',
    },
    {
      id: 13,
      info: 'Cirurgia Plástica',
    },
    {
      id: 14,
      info: 'Cirurgia Torácica',
    },
    {
      id: 15,
      info: 'Cirurgia Vascular',
    },
    {
      id: 16,
      info: 'Clínica Médica (Medicina interna)',
    },
    {
      id: 17,
      info: 'Coloproctologia',
    },
    {
      id: 18,
      info: 'Dermatologia',
    },
    {
      id: 19,
      info: 'Endocrinologia e Metabologia',
    },
    {
      id: 20,
      info: 'Endoscopia',
    },
    {
      id: 21,
      info: 'Gastroenterologia',
    },
    {
      id: 22,
      info: 'Genética médica',
    },
    {
      id: 23,
      info: 'Geriatria',
    },
    {
      id: 24,
      info: 'Ginecologia e obstetrícia',
    },
    {
      id: 25,
      info: 'Hematologia e Hemoterapia',
    },
    {
      id: 26,
      info: 'Homeopatia',
    },
    {
      id: 27,
      info: 'Infectologia',
    },
    {
      id: 28,
      info: 'Mastologia',
    },
    {
      id: 29,
      info: 'Medicina de Família e Comunidade',
    },
    {
      id: 30,
      info: 'Medicina de Emergência',
    },
    {
      id: 31,
      info: 'Medicina do Trabalho',
    },
    {
      id: 32,
      info: 'Medicina do Tráfego',
    },
    {
      id: 33,
      info: 'Medicina Esportiva',
    },
    {
      id: 34,
      info: 'Medicina Física e Reabilitação',
    },
    {
      id: 35,
      info: 'Medicina Intensiva',
    },
    {
      id: 36,
      info: 'Medicina Legal e Perícia Médica (ou medicina forense)',
    },
    {
      id: 37,
      info: 'Medicina Nuclear',
    },
    {
      id: 38,
      info: 'Medicina Preventiva e Social',
    },
    {
      id: 39,
      info: 'Nefrologia',
    },
    {
      id: 40,
      info: 'Neurocirurgia',
    },
    {
      id: 41,
      info: 'Neurologia',
    },
    {
      id: 42,
      info: 'Nutrologia',
    },
    {
      id: 43,
      info: 'Obstetrícia',
    },
    {
      id: 44,
      info: 'Oftalmologia',
    },
    {
      id: 45,
      info: 'Oncologia',
    },
    {
      id: 46,
      info: 'Ortopedia e Traumatologia',
    },
    {
      id: 47,
      info: 'Otorrinolaringologia',
    },
    {
      id: 46,
      info: 'Patologia',
    },
    {
      id: 47,
      info: 'Patologia Clínica / Medicina laboratorial',
    },
    {
      id: 48,
      info: 'Pediatria',
    },
    {
      id: 49,
      info: 'Pneumologia',
    },
    {
      id: 50,
      info: 'Psiquiatria',
    },
    {
      id: 51,
      info: 'Radiologia e Diagnóstico por Imagem',
    },
    {
      id: 52,
      info: 'Radioterapia',
    },
    {
      id: 53,
      info: 'Reumatologia',
    },
    {
      id: 54,
      info: 'Urologia',
    }
  ];
}
