import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { ModalModule } from 'ngx-bootstrap/modal';
import { DataTablesModule } from 'angular-datatables';
import { NgxMaskModule } from 'ngx-mask';

import { LogoutComponent } from './logout/logout.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangePasswordSafelyComponent } from './change-password-safely/change-password-safely.component';
import { CepService } from './service/cep.service';
import { UtilsService } from './service/utils.service';
import { RefreshTokenService } from './service/refresh-token.service';
// Patients
import { ListPatientsComponent } from './patients/list-patients/list-patients.component';
import { RegisterPatientsComponent } from './patients/register-patients/register-patients.component';
import { UpdatePatientsComponent } from './patients/update-patients/update-patients.component';
import { UpdateStatusPatientsComponent } from './patients/update-status-patients/update-status-patients.component';
import { ListHistoricsComponent } from './patients/list-historics/list-historics.component';

@NgModule({
  declarations: [
    LogoutComponent,
    ChangePasswordComponent,
    ChangePasswordSafelyComponent,
    ListPatientsComponent,
    RegisterPatientsComponent,
    UpdatePatientsComponent,
    UpdateStatusPatientsComponent,
    ListHistoricsComponent,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    DataTablesModule,
    NgxMaskModule.forRoot(),
  ],
  exports: [
    LogoutComponent,
    ChangePasswordComponent,
    ChangePasswordSafelyComponent,
  ],
  providers: [
    CepService,
    UtilsService,
    RefreshTokenService,
  ]
})
export class SharedModule { }
