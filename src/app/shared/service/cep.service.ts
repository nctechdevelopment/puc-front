import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CepService {

  constructor(private http: HttpClient) { }

  searchCep(cep: string): Observable<any> {
    return this.http.get(`//viacep.com.br/ws/${cep}/json`).pipe(map((result: Array<any>) => result));
  }

}
