import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RefreshTokenService {

  url: string;
  eventRefreshToken: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/access`;
  }

  emitEventRefreshToken() {
    this.eventRefreshToken.emit();
  }

  getEventRefreshToken() {
    return this.eventRefreshToken;
  }

  refreshToken(): Observable<any> {
    return this.http.get(`${this.url}/refresh-token`).pipe(map((result: any) => result));
  }

}
