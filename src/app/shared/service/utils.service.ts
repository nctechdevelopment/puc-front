import { Injectable } from '@angular/core';

import * as moment from 'moment';
moment.locale('pt-BR');

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  calculateAge(birthDate: string, today: string) {
    const [day, month, year] = birthDate.split('/').map(elem => parseInt(elem, 10));
    let [currentDay, currentMonth, currentYear] = today.split('/').map(elem => parseInt(elem, 10));
    const numberDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if (day > currentDay) {
      currentDay = currentDay + numberDays[month - 1];
      currentMonth = currentMonth - 1;
    }

    if (month > currentMonth) {
      currentMonth = currentMonth + 12;
      currentYear = currentYear - 1;
    }

    const calculatedDay = currentDay - day;
    const calculatedMonth = currentMonth - month;
    const calculatedYear = currentYear - year;

    return (calculatedYear > 2 ? calculatedYear + ' Anos, ' : calculatedYear + ' Ano, ') +
      (calculatedMonth > 2 ? calculatedMonth + ' Meses e ' : calculatedMonth + ' Mês e ') +
      (calculatedDay > 2 ? calculatedDay + ' Dias' : calculatedDay + ' Dia');
  }

  subtractingDatesInDays(date: any) {
    const a = new Date();
    const b = new Date(date);

    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate() + 1);

    return Math.floor((utc2 - utc1) / 86400000);
  }

  getClassToStatus(status: any) {
    let result;

    switch (status) {
      case '0': result = 'status-agendado'; break;
      case '1': result = 'status-atendido'; break;
      case '2': result = 'status-ausente'; break;
    }

    return result;
  }

}
