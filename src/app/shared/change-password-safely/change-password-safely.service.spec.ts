import { TestBed } from '@angular/core/testing';

import { ChangePasswordSafelyService } from './change-password-safely.service';

describe('ChangePasswordSafelyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChangePasswordSafelyService = TestBed.get(ChangePasswordSafelyService);
    expect(service).toBeTruthy();
  });
});
