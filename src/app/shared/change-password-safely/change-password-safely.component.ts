import { Component, ViewChild } from '@angular/core';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { LoginModel } from '../../models/login.model';
import { ChangePasswordSafelyService } from './change-password-safely.service';

@Component({
  selector: 'app-change-password-safely',
  templateUrl: './change-password-safely.component.html',
  styleUrls: ['./change-password-safely.component.css'],
  providers: [ChangePasswordSafelyService]
})
export class ChangePasswordSafelyComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  loginId: number;
  user: LoginModel;
  differentPasswords: boolean;
  isModalShown = false;

  constructor(
    private changePasswordSafelyService: ChangePasswordSafelyService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
  }

  public changePasswordSafely() {
    this.changePasswordSafelyService.changePasswordSafely(this.loginId, this.user).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.closeModal();
    }, (error) => {
      console.log(error);
      const toast: Toast = {
        type: 'error',
        title: 'Ops!',
        body: error.error.message,
        bodyOutputType: BodyOutputType.TrustedHtml
      };
      this.toaster.pop(toast);
    });
  }

  public openModal(loginId: number) {
    this.loginId = loginId;
    this.isModalShown = true;
  }

  public closeModal() {
    this.user = new LoginModel();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public validatePasswords() {
    this.differentPasswords = this.user.novaSenha !== this.user.confirmarSenha;
  }

}
