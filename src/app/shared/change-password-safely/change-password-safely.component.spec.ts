import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordSafelyComponent } from './change-password-safely.component';

describe('ChangePasswordSafelyComponent', () => {
  let component: ChangePasswordSafelyComponent;
  let fixture: ComponentFixture<ChangePasswordSafelyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePasswordSafelyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordSafelyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
