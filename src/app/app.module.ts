import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToasterModule, ToasterService } from 'angular2-toaster';

import { TokenInterceptorService } from './auth/token-interceptor.service';
import { TokenErrorInterceptorService } from './auth/token-error-interceptor.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginExamComponent } from './login-exam/login-exam.component';
import { HomeComponent } from './home/home.component';

// Dashboards
import { DashboardModule } from './dashboard/dashboard.module';
import { DashboardDoctorModule } from './dashboard-doctor/dashboard-doctor.module';
import { DashboardLaboratoryModule } from './dashboard-laboratory/dashboard-laboratory.module';
import { DashboardReceptionistModule } from './dashboard-receptionist/dashboard-receptionist.module';
import { DashboardPatientModule } from './dashboard-patient/dashboard-patient.module';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginExamComponent,
    HomeComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DashboardModule,
    DashboardDoctorModule,
    DashboardLaboratoryModule,
    DashboardReceptionistModule,
    DashboardPatientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule,
  ],
  providers: [
    HttpClient,
    HttpClientModule,
    ToasterService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenErrorInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
