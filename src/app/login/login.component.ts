import { Component } from '@angular/core';

import { ToasterService } from 'angular2-toaster';

import { AuthGuard } from '../auth/auth.guard';
import { LoginService } from './login.service';
import { LoginModel } from '../models/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent {

  user: LoginModel;

  constructor(
    private loginService: LoginService,
    private toaster: ToasterService,
    private authGuard: AuthGuard,
  ) {
    this.user = new LoginModel();
  }

  public login() {
    this.loginService.login(this.user).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', 'Login realizado com sucesso!');

      this.authGuard.setToken(result.token);
      switch (this.authGuard.getAccessLevel()) {
        case '1':
          window.location.href = '/painel-admin';
          break;
        case '2':
          window.location.href = '/painel-laboratorio';
          break;
        case '3':
          window.location.href = '/painel-medico';
          break;
        case '4':
          window.location.href = '/painel-recepcionista';
          break;
        default:
          window.location.href = '/';
          break;
      }
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

}
