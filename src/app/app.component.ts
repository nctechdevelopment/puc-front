import { Component } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'puc-front';

  toasterconfig = new ToasterConfig({
    showCloseButton: false,
    tapToDismiss: true,
    timeout: 5000,
    iconClasses: {
      error: 'icon-error',
      info: 'icon-info',
      wait: 'icon-wait',
      success: 'icon-success',
      warning: 'icon-warning'
    },
    titleClass: 'toaster-title',
    messageClass: 'toaster-text',
    preventDuplicates: true,
    limit: 5,
  });

  constructor() {
    setTheme('bs4');
  }

}
