import { TestBed } from '@angular/core/testing';

import { DashboardLaboratoryService } from './dashboard-laboratory.service';

describe('DashboardLaboratoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardLaboratoryService = TestBed.get(DashboardLaboratoryService);
    expect(service).toBeTruthy();
  });
});
