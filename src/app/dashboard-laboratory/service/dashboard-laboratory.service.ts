import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { LoginModel } from 'src/app/models/login.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardLaboratoryService {

  url: string;
  urlLaboratory: string;
  urlMedical: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.BASE_URL}`;
    this.urlLaboratory = `${environment.API_URL}/laboratory`;
    this.urlMedical = `${environment.API_URL}/medical`;
  }

  update(fd: FormData): Observable<any> {
    return this.http.post(`${this.urlLaboratory}/update`, fd).pipe(map((result: any) => result));
  }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.urlLaboratory}/${id}`).pipe(map((result: any) => result));
  }

  loginExam(user: LoginModel): Observable<any> {
    return this.http.post(this.url + '/access/login/exam', user).pipe(map((result: any) => result));
  }

  uploadExam(data: FormData): Observable<any> {
    return this.http.post(`${this.urlMedical}/upload/exam`, data).pipe(map((result: any) => result));
  }

  downloadExam(id: number): Observable<any> {
    return this.http.get(`${this.urlMedical}/download/exam/${id}`).pipe(map((result: any) => result));
  }

}
