import { Component } from '@angular/core';
import { ToasterService } from 'angular2-toaster';

import { AuthGuard } from '../../auth/auth.guard';
import { LoginModel } from '../../models/login.model';
import { DashboardLaboratoryService } from '../service/dashboard-laboratory.service';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.css'],
  providers: [DashboardLaboratoryService]
})
export class ExamsComponent {

  user: LoginModel;
  exam: any;

  selectedFile: File;
  selectedFilename: string;

  constructor(
    private authGuard: AuthGuard,
    private dashboardLaboratoryService: DashboardLaboratoryService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
  }

  public loginExam() {
    this.exam = null;

    this.dashboardLaboratoryService.loginExam(this.user).subscribe((result: any) => {
      this.user = this.authGuard.getDecodeToken(result.token);

      this.downloadExam();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public downloadExam() {
    this.dashboardLaboratoryService.downloadExam(this.user.id).subscribe((result: any) => {
      this.exam = result.exams[0];
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public uploadExam() {
    const fd = new FormData();
    fd.append('id', this.user.id.toString());
    fd.append('file', this.selectedFile, this.selectedFile.name);

    this.dashboardLaboratoryService.uploadExam(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.downloadExam();
      this.selectedFile = null;
      this.selectedFilename = null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

}
