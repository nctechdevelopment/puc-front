import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { AuthGuard } from '../../auth/auth.guard';
import { LoginModel } from '../../models/login.model';
import { LaboratoryModel } from '../../models/laboratory.model';
import { DashboardLaboratoryService } from '../service/dashboard-laboratory.service';
import { CepService } from '../../shared/service/cep.service';
import { RefreshTokenService } from '../../shared/service/refresh-token.service';
import { ChangePasswordSafelyComponent } from '../../shared/change-password-safely/change-password-safely.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [DashboardLaboratoryService]
})
export class ProfileComponent implements OnInit {

  @ViewChild('modalChangePasswordSafely', { static: false }) public modalChangePasswordSafely: ChangePasswordSafelyComponent;

  user: LoginModel;
  laboratory: LaboratoryModel;
  selectedFile: File;
  selectedFilename: string;

  constructor(
    private authGuard: AuthGuard,
    private cepService: CepService,
    private refreshTokenService: RefreshTokenService,
    private dashboardLaboratoryService: DashboardLaboratoryService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
    this.laboratory = new LaboratoryModel();
  }

  ngOnInit() {
    this.user = this.authGuard.getUser();

    this.findById();
  }

  public findById() {
    this.dashboardLaboratoryService.findById(this.user.id).subscribe((result: any) => {
      this.laboratory = result.laboratory;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.laboratory.imagemAntiga) { fd.append('imagemAntiga', this.laboratory.imagemAntiga); }
    fd.append('id', this.laboratory.id.toString());
    fd.append('nome', this.laboratory.nome);
    fd.append('razaoSocial', this.laboratory.razaoSocial);
    fd.append('cnpj', this.laboratory.cnpj);
    fd.append('cep', this.laboratory.cep);
    fd.append('estado', this.laboratory.estado);
    fd.append('cidade', this.laboratory.cidade);
    fd.append('bairro', this.laboratory.bairro);
    fd.append('logradouro', this.laboratory.logradouro);
    if (this.laboratory.complemento) { fd.append('complemento', this.laboratory.complemento); }
    fd.append('numero', this.laboratory.numero);
    fd.append('celular', this.laboratory.celular);
    if (this.laboratory.telefone) { fd.append('telefone', this.laboratory.telefone); }

    this.dashboardLaboratoryService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.refreshTokenService.emitEventRefreshToken();
      this.selectedFile = null;
      this.selectedFilename = '';
      this.findById();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModalChangePasswordSafely() {
    this.modalChangePasswordSafely.openModal(this.user.loginId);
  }

  public searchCep() {
    this.cepService.searchCep(this.laboratory.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.laboratory.complemento = result.complemento ? result.complemento : null;
      this.laboratory.logradouro = result.logradouro ? result.logradouro : null;
      this.laboratory.bairro = result.bairro ? result.bairro : null;
      this.laboratory.cidade = result.localidade ? result.localidade : null;
      this.laboratory.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

}
