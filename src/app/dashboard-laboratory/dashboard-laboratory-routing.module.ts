import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { PermissionGuard } from '../auth/permission.guard';
import { DashboardLaboratoryComponent } from './dashboard-laboratory.component';
import { ProfileComponent } from './profile/profile.component';
import { ExamsComponent } from './exams/exams.component';

const routes: Routes = [
  {
    path: 'painel-laboratorio',
    canActivate: [AuthGuard, PermissionGuard],
    data: { roles: ['2'] },
    component: DashboardLaboratoryComponent,
    children: [
      {
        path: 'perfil',
        canActivate: [PermissionGuard],
        data: { roles: ['2'] },
        component: ProfileComponent
      },
      {
        path: 'exames',
        canActivate: [PermissionGuard],
        data: { roles: ['2'] },
        component: ExamsComponent
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardLaboratoryRoutingModule { }
