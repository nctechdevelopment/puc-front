import { Component } from '@angular/core';

import { AuthGuard } from '../auth/auth.guard';
import { LoginModel } from '../models/login.model';
import { RefreshTokenService } from '../shared/service/refresh-token.service';

@Component({
  selector: 'app-dashboard-laboratory',
  templateUrl: './dashboard-laboratory.component.html',
  styleUrls: ['./dashboard-laboratory.component.css']
})
export class DashboardLaboratoryComponent {

  user: LoginModel;

  constructor(
    private authGuard: AuthGuard,
    private refreshTokenService: RefreshTokenService,
  ) {
    this.user = this.authGuard.getUser();

    this.refreshTokenService.getEventRefreshToken().subscribe(() => this.refreshToken());
  }

  public refreshToken() {
    this.refreshTokenService.refreshToken().subscribe((result: any) => {
      this.authGuard.setToken(result.token);
      this.user = this.authGuard.getUser();
    }, (error) => console.log(error));
  }

}
