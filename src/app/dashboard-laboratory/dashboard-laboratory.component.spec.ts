import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardLaboratoryComponent } from './dashboard-laboratory.component';

describe('DashboardLaboratoryComponent', () => {
  let component: DashboardLaboratoryComponent;
  let fixture: ComponentFixture<DashboardLaboratoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardLaboratoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLaboratoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
