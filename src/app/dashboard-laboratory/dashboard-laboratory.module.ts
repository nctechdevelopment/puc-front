import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxMaskModule } from 'ngx-mask';

import { DashboardLaboratoryRoutingModule } from './dashboard-laboratory-routing.module';
import { DashboardLaboratoryComponent } from './dashboard-laboratory.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { ExamsComponent } from './exams/exams.component';

@NgModule({
  declarations: [
    DashboardLaboratoryComponent,
    ProfileComponent,
    ExamsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardLaboratoryRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
  ]
})
export class DashboardLaboratoryModule { }
