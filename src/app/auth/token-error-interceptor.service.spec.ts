import { TestBed } from '@angular/core/testing';

import { TokenErrorInterceptorService } from './token-error-interceptor.service';

describe('TokenErrorInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TokenErrorInterceptorService = TestBed.get(TokenErrorInterceptorService);
    expect(service).toBeTruthy();
  });
});
