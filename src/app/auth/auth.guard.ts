import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToasterService } from 'angular2-toaster';

import { LoginModel } from '../models/login.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private user: LoginModel;
  private jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private router: Router, private toaster: ToasterService) {
    this.user = new LoginModel();
    this.setUser();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = this.getToken();
    if (token) {
      return true;
    } else {
      this.toaster.pop('error', 'ACESSO NEGADO!', 'Você deve se conectar para continuar.');
      this.router.navigate(['/']);
      return false;
    }
  }

  public getUser() {
    return this.user;
  }

  public getAccessLevel() {
    return this.user.nivelAcesso;
  }

  public setUser() {
    const token = this.getToken();
    this.user = token ? this.jwtHelper.decodeToken(token) : new LoginModel();
  }

  public getDecodeToken(token: string) {
    return this.jwtHelper.decodeToken(token);
  }

  public getToken(): string {
    return localStorage.getItem('@puc_token');
  }

  public setToken(token: string) {
    localStorage.setItem('@puc_token', token);
    this.setUser();
  }

  public removeToken(): void {
    localStorage.clear();
  }

}
