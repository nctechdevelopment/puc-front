import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthGuard } from './auth.guard';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private authGuard: AuthGuard) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.includes('viacep')) {
      request = request.clone({
        setHeaders: { Authorization: `Bearer ${this.authGuard.getToken()}` }
      });
    }
    return next.handle(request);
  }

}
