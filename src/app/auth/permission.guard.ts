import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ToasterService } from 'angular2-toaster';

import { AuthGuard } from './auth.guard';
import { LoginModel } from '../models/login.model';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

  private user: LoginModel;

  constructor(
    private router: Router,
    private authGuard: AuthGuard,
    private toaster: ToasterService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.user = this.authGuard.getUser();
    if (next.data.roles.indexOf(this.user.nivelAcesso) >= 0) {
      return true;
    } else {
      this.toaster.pop('error', 'ACESSO NEGADO!', 'Você não tem permissão para acessar está página, consulte o administrador.');
      switch (this.user.nivelAcesso) {
        case '1':
          this.router.navigate(['/painel-admin']);
          return false;
        case '2':
          this.router.navigate(['/painel-laboratorio']);
          return false;
        case '3':
          this.router.navigate(['/painel-medico']);
          return false;
        case '4':
          this.router.navigate(['/painel-recepcionista']);
          return false;
        default:
          this.router.navigate(['/']);
          return false;
      }
    }
  }

}
