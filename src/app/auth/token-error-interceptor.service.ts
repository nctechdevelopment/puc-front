import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthGuard } from './auth.guard';

@Injectable({
  providedIn: 'root'
})
export class TokenErrorInterceptorService implements HttpInterceptor {

  constructor(
    private router: Router,
    private authGuard: AuthGuard
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (error.status === 401 && error.error.token_error) {
          this.authGuard.removeToken();
          this.router.navigate(['/']);
        }

        if (error.status === 403 && error.error.authorized_error) {
          this.authGuard.removeToken();
          this.router.navigate(['/']);
        }

        throw error;
      })
    );
  }

}
