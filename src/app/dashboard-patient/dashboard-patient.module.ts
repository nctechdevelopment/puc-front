import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DashboardPatientRoutingModule } from './dashboard-patient-routing.module';
import { DashboardPatientComponent } from './dashboard-patient.component';
import { SharedModule } from '../shared/shared.module';
import { DownloadComponent } from './download/download.component';

@NgModule({
  declarations: [
    DashboardPatientComponent,
    DownloadComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardPatientRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DashboardPatientModule { }
