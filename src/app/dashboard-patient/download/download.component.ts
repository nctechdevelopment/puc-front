import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';

import { AuthGuard } from '../../auth/auth.guard';
import { LoginModel } from '../../models/login.model';
import { DashboardPatientService } from '../service/dashboard-patient.service';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css'],
  providers: [DashboardPatientService]
})
export class DownloadComponent implements OnInit {

  exam: any;
  user: LoginModel;

  constructor(
    private authGuard: AuthGuard,
    private dashboardPatientService: DashboardPatientService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
  }

  ngOnInit() {
    this.user = this.authGuard.getUser();
    this.downloadExam();
  }

  public downloadExam() {
    this.dashboardPatientService.downloadExam(this.user.id).subscribe((result: any) => {
      this.exam = result.exams[0];
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

}
