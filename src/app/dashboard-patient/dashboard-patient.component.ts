import { Component } from '@angular/core';

import { AuthGuard } from '../auth/auth.guard';
import { LoginModel } from '../models/login.model';

@Component({
  selector: 'app-dashboard-patient',
  templateUrl: './dashboard-patient.component.html',
  styleUrls: ['./dashboard-patient.component.css']
})
export class DashboardPatientComponent {

  user: LoginModel;

  constructor(
    private authGuard: AuthGuard,
  ) {
    this.user = this.authGuard.getUser();
  }

}
