import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { PermissionGuard } from '../auth/permission.guard';
import { DashboardPatientComponent } from './dashboard-patient.component';

const routes: Routes = [
  {
    path: 'painel-paciente',
    canActivate: [AuthGuard, PermissionGuard],
    data: { roles: ['5'] },
    component: DashboardPatientComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardPatientRoutingModule { }
