import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardPatientService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/medical`;
  }

  downloadExam(id: number): Observable<any> {
    return this.http.get(`${this.url}/download/exam/${id}`).pipe(map((result: any) => result));
  }

}
