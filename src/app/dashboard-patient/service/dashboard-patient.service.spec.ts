import { TestBed } from '@angular/core/testing';

import { DashboardPatientService } from './dashboard-patient.service';

describe('DashboardPatientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardPatientService = TestBed.get(DashboardPatientService);
    expect(service).toBeTruthy();
  });
});
