import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { MedicalRecordModel } from '../../models/medical-record.model';
import { MedicalService } from './service/medical.service';
import { PatientModel } from '../../models/patient.model';
import { PatientsService } from '../../shared/patients/service/patients.service';
import { ScheduleModel } from '../../models/schedule.model';
import { SchedulesService } from '../schedules/service/schedules.service';
import { ExamExamTypeModel, ExamTypeModel } from '../../models/exam.model';
import { CertificateModel } from '../../models/certificate.model';
import { PrescriptionModel } from '../../models/medicament.model';
import { UtilsService } from '../../shared/service/utils.service';
import { Settings } from '../../shared/settings/settings';
import { PdfComponent } from '../pdf/pdf/pdf.component';
import { ExamComponent } from '../historic/exam/exam.component';
import { CertificateComponent } from '../historic/certificate/certificate.component';
import { PrescriptionComponent } from '../historic/prescription/prescription.component';
import { MedicalRecordComponent } from '../historic/medical-record/medical-record.component';

@Component({
  selector: 'app-medical',
  templateUrl: './medical.component.html',
  styleUrls: ['./medical.component.css'],
  providers: [MedicalService, SchedulesService]
})
export class MedicalComponent implements OnInit {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;
  @ViewChild('modalPdf', { static: false }) public modalPdf: PdfComponent;
  @ViewChild('modalExamHistoric', { static: false }) public modalExamHistoric: ExamComponent;
  @ViewChild('modalCertificateHistoric', { static: false }) public modalCertificateHistoric: CertificateComponent;
  @ViewChild('modalPrescriptionHistoric', { static: false }) public modalPrescriptionHistoric: PrescriptionComponent;
  @ViewChild('modalMedicalRecordHistoric', { static: false }) public modalMedicalRecordHistoric: MedicalRecordComponent;

  imc: number;
  menuIndex = 1;
  finished = false;
  isModalShown = false;

  schedulesId: string;

  patient: PatientModel;
  schedule: ScheduleModel;
  medicalExam: Array<ExamExamTypeModel>;
  medicalRecord: MedicalRecordModel;
  medicalPrescription: Array<PrescriptionModel>;
  medicalCertificate: CertificateModel;

  timeout = null;
  dropdownSettings = {};
  typesExams: Array<Array<{ id: number, info: string }>>;
  selectedTypesExams: Array<Array<{ id: number, info: string }>>;
  medicaments: Array<{
    id: number,
    info: string,
    nomeGenerico: string,
    nomeFabrica: string,
    nomeFabricante: string,
    posologia: string
  }>;
  selectedMedicaments: Array<{
    id: number,
    info: string,
    nomeGenerico: string,
    nomeFabrica: string,
    nomeFabricante: string,
    posologia: string
  }>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private utilsService: UtilsService,
    private patientsService: PatientsService,
    private medicalService: MedicalService,
    private schedulesService: SchedulesService,
    private toaster: ToasterService,
  ) {
    this.patient = new PatientModel();
    this.schedule = new ScheduleModel();
    this.medicalExam = new Array<ExamExamTypeModel>();
    this.medicalRecord = new MedicalRecordModel();
    this.medicalCertificate = new CertificateModel();
    this.medicalPrescription = new Array<PrescriptionModel>();

    this.medicaments = new Array<{
      id: number,
      info: string,
      nomeGenerico: string,
      nomeFabrica: string,
      nomeFabricante: string,
      posologia: string
    }>();
    this.selectedMedicaments = new Array<{
      id: number,
      info: string,
      nomeGenerico: string,
      nomeFabrica: string,
      nomeFabricante: string,
      posologia: string
    }>();
    this.typesExams = new Array<Array<{ id: number, info: string }>>();
    this.selectedTypesExams = new Array<Array<{ id: number, info: string }>>();
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => this.schedulesId = res.id);
    this.dropdownSettings = Object.assign({ singleSelection: true }, Settings.dropdown);
    this.findScheduleById();
  }

  public showMenu(index: number) {
    this.menuIndex = this.menuIndex === -1 ? -1 : index;
  }

  public calcImc() {
    if (this.medicalRecord.peso && this.medicalRecord.altura) {
      this.imc = this.medicalRecord.peso / (this.medicalRecord.altura * this.medicalRecord.altura);
    }
  }

  // Finds
  public findScheduleById() {
    this.schedulesService.findById(this.schedulesId).subscribe((result: any) => {
      this.schedule = result.schedule;
      this.findPatientById();

      const date = result.schedule.dataInicio.split(' ')[0];
      if (result.schedule.status !== '0' ||
        this.utilsService.subtractingDatesInDays(date) < 0) {
        this.menuIndex = 0;
        this.finished = true;
        this.findRecordByScheduleId();
      }
    }, (error) => {
      console.log(error);
      if (error.status === 404) { this.menuIndex = -1; }
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findRecordByScheduleId() {
    this.medicalService.findRecordByScheduleId(this.schedule.id).subscribe((result: any) => {
      this.medicalRecord = result.record;
      this.calcImc();

      this.findExamByRecordId();
      this.findPrescriptionByRecordId();
      this.findCertificateByRecordId();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findExamByRecordId() {
    this.medicalService.findExamByRecordId(this.medicalRecord.id).subscribe((result: any) => {
      this.medicalExam = result.exams;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findPrescriptionByRecordId() {
    this.medicalService.findPrescriptionByRecordId(this.medicalRecord.id).subscribe((result: any) => {
      this.medicalPrescription = result.prescriptions;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findCertificateByRecordId() {
    this.medicalService.findCertificateByRecordId(this.medicalRecord.id).subscribe((result: any) => {
      this.medicalCertificate = result.certificate;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public findPatientById() {
    this.patientsService.findById(this.schedule.paciente.id).subscribe((result: any) => {
      this.patient = result.patient;
      this.patient.idade = this.utilsService.calculateAge(
        moment(result.patient.dataNascimento).format('DD/MM/YYYY'),
        moment().format('DD/MM/YYYY')
      );
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  // Registers
  public validateMedical() {
    if (this.finished) {
      this.toaster.pop('success', 'Sucesso!', 'Atendimento finalizado.');
    } else if (!this.schedule.id) {
      this.toaster.pop('error', 'Ops!', 'Agendamento não encontrado.');
    } else {
      const length = this.medicalExam.length;
      if (length) {
        this.medicalExam.map((elem, index) => {
          if (!elem.tipoExame.length) {
            this.toaster.pop('error', 'Ops!', 'Não é possível cadastrar exames vazios.');
            return;
          }

          if (length === (index + 1)) {
            this.registerMedical();
          }
        });
      } else { this.registerMedical(); }
    }
  }

  public registerMedical() {
    this.medicalRecord.agendamento.id = this.schedule.id;
    this.medicalRecord.paciente.id = this.schedule.paciente.id;
    this.medicalRecord.medico.id = this.schedule.medico.id;

    const obj = {
      record: this.medicalRecord,
      prescription: this.medicalPrescription.length ? this.medicalPrescription : false,
      exam: this.medicalExam.length ? this.medicalExam : false,
      certificate: this.medicalCertificate.periodo ? this.medicalCertificate : false,
    };

    this.medicalService.registerMedical(obj).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.finished = true;
      this.menuIndex = 0;
      this.findRecordByScheduleId();
      this.closeModal();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  // Medicaments
  public onKeyUpMedicaments(args: any) {
    const value = args.target.value.toLowerCase();

    if (value.length === 0) { return; }

    clearTimeout(this.timeout);

    if (args.keyCode === 13) {
      this.searchMedicaments(value);
    } else { this.timeout = setTimeout(() => this.searchMedicaments(value), 1000); }
  }

  public searchMedicaments(data: string) {
    this.medicalService.searchMedicaments(data).subscribe((result: any) => {
      this.medicaments = [];
      result.medicaments.map((elem, i) => {
        this.medicaments[i] = {
          id: elem.id,
          info: `${elem.nome_generico ? elem.nome_generico : ''}
            ${elem.nome_generico && elem.nome_fabrica ? ' - ' : ''}
            ${elem.nome_fabrica ? elem.nome_fabrica : ''}`,
          nomeGenerico: elem.nome_generico,
          nomeFabrica: elem.nome_fabrica,
          nomeFabricante: elem.nome_fabricante,
          posologia: null,
        };
      });
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public onDeSelectMedicaments() {
    this.selectedMedicaments = [];
  }

  public addMedicaments() {
    this.medicalPrescription.push({
      id: null,
      posologia: this.selectedMedicaments[0].posologia,
      medicamento: {
        id: this.selectedMedicaments[0].id,
        nomeGenerico: this.selectedMedicaments[0].nomeGenerico,
        nomeFabrica: this.selectedMedicaments[0].nomeFabrica,
        nomeFabricante: this.selectedMedicaments[0].nomeFabricante,
      },
      prontuario: null,
    });

    this.medicaments = [];
    this.selectedMedicaments = [];
    $('#inputSearchMedicaments').val('');
  }

  public removeMedicaments(i: number) {
    this.medicalPrescription = this.medicalPrescription.filter((elem, index) => {
      if (index !== i) { return elem; }
    });
  }

  // Exams
  public onKeyUpExams(args: any, indexExam: number) {
    const value = args.target.value.toLowerCase();

    if (value.length === 0) { return; }

    clearTimeout(this.timeout);

    if (args.keyCode === 13) {
      this.searchExamTypes(value, indexExam);
    } else { this.timeout = setTimeout(() => this.searchExamTypes(value, indexExam), 1000); }
  }

  public searchExamTypes(data: string, indexExam: number) {
    this.medicalService.searchExamTypes(data).subscribe((result: any) => {
      this.typesExams[indexExam] = [];
      result.exams.map((elem, i) => {
        this.typesExams[indexExam][i] = {
          id: elem.id,
          info: elem.nome,
        };
      });
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public onDeSelectExams(indexExam: number) {
    this.selectedTypesExams[indexExam] = [];
  }

  public createExams() {
    this.typesExams.push([]);
    this.selectedTypesExams.push([]);

    this.medicalExam.push({
      id: null,
      exame: null,
      tipoExame: new Array<ExamTypeModel>(),
    });
  }

  public deleteExam(indexExam: number) {
    this.medicalExam = this.medicalExam.filter((elem, index) => {
      if (index !== indexExam) { return elem; }
    });
  }

  public addExams(indexExam: number) {
    this.medicalExam[indexExam].tipoExame.push({
      id: this.selectedTypesExams[indexExam][0].id,
      nome: this.selectedTypesExams[indexExam][0].info
    });

    this.typesExams[indexExam] = [];
    this.selectedTypesExams[indexExam] = [];
    $(`#inputSearchExamsTypes_${indexExam}`).val('');
  }

  public removeExams(indexExam: number, indexTypeExam: number) {
    this.medicalExam[indexExam].tipoExame = this.medicalExam[indexExam].tipoExame.filter((elem, index) => {
      if (index !== indexTypeExam) { return elem; }
    });
  }

  // Modals
  public openModal() {
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

  public openModalPdf(option: string) {
    switch (option) {
      case 'certificate': {
        this.modalPdf.openModal(option, this.medicalRecord, this.medicalCertificate);
        break;
      }
      case 'exam': {
        this.modalPdf.openModal(option, this.medicalRecord, this.medicalExam);
        break;
      }
      case 'prescription': {
        this.modalPdf.openModal(option, this.medicalRecord, this.medicalPrescription);
        break;
      }
    }
  }

  public openModalExamHistoric() {
    this.modalExamHistoric.openModal(this.patient.id);
  }

  public openModalCertificateHistoric() {
    this.modalCertificateHistoric.openModal(this.patient.id);
  }

  public openModalPrescriptionHistoric() {
    this.modalPrescriptionHistoric.openModal(this.patient.id);
  }

  public openModalMedicalRecordHistoric() {
    this.modalMedicalRecordHistoric.openModal(this.patient.id);
  }

}
