import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { PatientModel } from '../../../models/patient.model';

@Injectable({
  providedIn: 'root'
})
export class MedicalService {

  urlExam: string;
  urlMedical: string;
  urlMedicament: string;

  constructor(private http: HttpClient) {
    this.urlExam = `${environment.API_URL}/exam`;
    this.urlMedical = `${environment.API_URL}/medical`;
    this.urlMedicament = `${environment.API_URL}/medicament`;
  }

  registerMedical(data: any): Observable<any> {
    return this.http.post(`${this.urlMedical}`, data).pipe(map((result: any) => result));
  }

  findRecordById(id: number): Observable<any> {
    return this.http.get(`${this.urlMedical}/record/${id}`).pipe(map((result: any) => result));
  }

  findRecordByScheduleId(id: number): Observable<any> {
    return this.http.get(`${this.urlMedical}/record/schedule/${id}`).pipe(map((result: any) => result));
  }

  findExamByRecordId(id: number): Observable<any> {
    return this.http.get(`${this.urlMedical}/exam/record/${id}`).pipe(map((result: any) => result));
  }

  findPrescriptionByRecordId(id: number): Observable<any> {
    return this.http.get(`${this.urlMedical}/prescription/record/${id}`).pipe(map((result: any) => result));
  }

  findCertificateByRecordId(id: number): Observable<any> {
    return this.http.get(`${this.urlMedical}/certificate/record/${id}`).pipe(map((result: any) => result));
  }

  searchExamTypes(search: string): Observable<any> {
    return this.http.get(`${this.urlMedical}/filter/exam-types/${search}`).pipe(map((result: any) => result));
  }

  searchMedicaments(search: string): Observable<any> {
    return this.http.get(`${this.urlMedicament}/filter/${search}`).pipe(map((result: any) => result));
  }

}
