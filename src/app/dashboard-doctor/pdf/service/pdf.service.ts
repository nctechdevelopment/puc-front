import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  urlMedical: string;

  constructor(private http: HttpClient) {
    this.urlMedical = `${environment.API_URL}/medical`;
  }

}
