import { Component, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as moment from 'moment';
moment.locale('pt-BR');

import { PdfService } from '../service/pdf.service';
import { Settings } from '../../../shared/settings/settings';
import { ExamExamTypeModel, ExamModel } from 'src/app/models/exam.model';
import { MedicalRecordModel } from 'src/app/models/medical-record.model';
import { PrescriptionModel } from 'src/app/models/medicament.model';
import { CertificateModel } from 'src/app/models/certificate.model';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css'],
  providers: [PdfService]
})
export class PdfComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  finished = false;
  isModalShown = false;

  medicalExam: Array<ExamExamTypeModel>;
  medicalRecord: MedicalRecordModel;
  medicalPrescription: Array<PrescriptionModel>;
  medicalCertificate: CertificateModel;

  constructor(
    private pdfService: PdfService,
    private toaster: ToasterService,
  ) {
    this.medicalExam = new Array<ExamExamTypeModel>();
    this.medicalRecord = new MedicalRecordModel();
    this.medicalCertificate = new CertificateModel();
    this.medicalPrescription = new Array<PrescriptionModel>();
  }

  public generatePDFCertificate() {
    const documentDefinition = {
      header: () => {
        return {
          table: {
            widths: ['*'],
            body: [
              [
                {
                  image: 'bot_head',
                  alignment: 'center'
                },
              ]
            ]
          },
          layout: 'noBorders',
          margin: [72, 40]
        };
      },
      footer: () => {
        return {
          table: {
            widths: ['*'],
            body: [
              [
                {
                  stack: [
                    {
                      text: [
                        { text: this.medicalRecord.medico.nome },
                        '\nCRM N° ', { text: this.medicalRecord.medico.crm },
                        '\n\n\n', { text: 'Gerado por Clínica Viva +' },
                      ],
                      alignment: 'center'
                    },
                  ],
                },
              ]
            ]
          },
          layout: 'noBorders',
          margin: [72, 40]
        };
      },
      content: [
        {
          text: 'Atestado Médico',
          fontSize: 18,
          alignment: 'center'
        },
        {
          stack: [
            {
              text: [
                'Paciente: ',
                { text: this.medicalRecord.paciente.nome },
              ],
              margin: [0, 80, 0, 0],
              fontSize: 14,
            },
          ],
        },
        {
          stack: [
            {
              text: [
                'Atesto, para os devidos fins, a pedido do interessado, que ',
                { text: this.medicalRecord.paciente.nome, bold: true },
                ' foi submetido à consulta médica nesta data, ',
                { text: moment(this.medicalRecord.agendamento.dataInicio).format('DD/MM/YYYY HH:mm:ss'), bold: true }, ' - ',
                { text: moment(this.medicalRecord.agendamento.dataFim).format('DD/MM/YYYY HH:mm:ss'), bold: true }, '.\n\n',
                'Em decorrência, deverá permanecer afastado de suas atividades laborativas por um período de ',
                { text: this.medicalCertificate.periodo, bold: true },
                ' , a partir desta data.',
              ],
              margin: [0, 150, 0, 0],
              fontSize: 14,
            },
          ],
        },
      ],
      images: {
        bot_head: Settings.logoBase64
      },
      pageSize: 'A4',
      pageMargins: [72, 150]
    };

    pdfMake.createPdf(documentDefinition).download('Atestado');
  }

  public generatePDFExam(exam: ExamExamTypeModel) {
    const myStack = exam.tipoExame.map((elem, index) => {
      return {
        text: [
          { text: `${index + 1}) ${elem.nome}\n` }
        ],
      };
    });

    const documentDefinition = {
      header: () => {
        return {
          table: {
            widths: ['*'],
            body: [
              [
                {
                  image: 'bot_head',
                  alignment: 'center'
                },
              ]
            ]
          },
          layout: 'noBorders',
          margin: [72, 40]
        };
      },
      footer: () => {
        return {
          table: {
            widths: ['*'],
            body: [
              [
                {
                  stack: [
                    {
                      text: [
                        { text: this.medicalRecord.medico.nome },
                        '\nCRM N° ', { text: this.medicalRecord.medico.crm },
                        '\n\n\n', { text: 'Gerado por Clínica Viva +' },
                      ],
                      alignment: 'center'
                    },
                  ],
                },
              ]
            ]
          },
          layout: 'noBorders',
          margin: [72, 40]
        };
      },
      content: [
        {
          text: 'Requisição',
          fontSize: 18,
          alignment: 'center'
        },
        {
          stack: [
            {
              text: [
                'Paciente: ',
                { text: this.medicalRecord.paciente.nome },
              ],
              margin: [0, 80, 0, 0],
              fontSize: 14,
            },
          ],
        },
        {
          stack: [
            {
              text: [
                'LOGIN: ', { text: exam.exame.login },
                '\nSENHA: ', { text: exam.exame.senha },
              ],
              margin: [0, 20, 0, 0],
              fontSize: 14,
            },
          ],
        },
        {
          stack: myStack,
          margin: [0, 60, 0, 0]
        }
      ],
      images: {
        bot_head: Settings.logoBase64
      },
      pageSize: 'A4',
      pageMargins: [72, 150]
    };

    pdfMake.createPdf(documentDefinition).download('Exame');
  }

  public generatePDFPrescription() {
    const myStack = this.medicalPrescription.map((elem, index) => {
      return {
        text: [
          { text: `${index + 1}) ${elem.medicamento.nomeGenerico ? elem.medicamento.nomeGenerico : ''} ${elem.medicamento.nomeGenerico && elem.medicamento.nomeFabrica ? ' - ' : ''} ${elem.medicamento.nomeFabrica ? elem.medicamento.nomeFabrica : ''}` },
          '\n', { text: elem.posologia }, '\n\n\n'
        ],
      };
    });

    const documentDefinition = {
      header: () => {
        return {
          table: {
            widths: ['*'],
            body: [
              [
                {
                  image: 'bot_head',
                  alignment: 'center'
                },
              ]
            ]
          },
          layout: 'noBorders',
          margin: [72, 40]
        };
      },
      footer: () => {
        return {
          table: {
            widths: ['*'],
            body: [
              [
                {
                  stack: [
                    {
                      text: [
                        { text: this.medicalRecord.medico.nome },
                        '\nCRM N° ', { text: this.medicalRecord.medico.crm },
                        '\n\n\n', { text: 'Gerado por Clínica Viva +' },
                      ],
                      alignment: 'center'
                    },
                  ],
                },
              ]
            ]
          },
          layout: 'noBorders',
          margin: [72, 40]
        };
      },
      content: [
        {
          text: 'Receita Médica',
          fontSize: 18,
          alignment: 'center'
        },
        {
          stack: [
            {
              text: [
                'Paciente: ',
                { text: this.medicalRecord.paciente.nome },
              ],
              margin: [0, 80, 0, 0],
              fontSize: 14,
            },
          ],
        },
        {
          stack: myStack,
          margin: [0, 50, 0, 0]
        }
      ],
      images: {
        bot_head: Settings.logoBase64
      },
      pageSize: 'A4',
      pageMargins: [72, 150]
    };

    pdfMake.createPdf(documentDefinition).download('Prescrição');
  }

  public openModal(option: any, record: any, data: any) {
    this.isModalShown = true;
    switch (option) {
      case 'certificate': {
        this.medicalRecord = record;
        this.medicalCertificate = data;
        this.generatePDFCertificate();
        break;
      }
      case 'exam': {
        this.medicalRecord = record;
        this.medicalExam = data;
        this.medicalExam.map(elem => this.generatePDFExam(elem));
        break;
      }
      case 'prescription': {
        this.medicalRecord = record;
        this.medicalPrescription = data;
        this.generatePDFPrescription();
        break;
      }
    }
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
