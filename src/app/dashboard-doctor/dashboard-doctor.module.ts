import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgxMaskModule } from 'ngx-mask';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FullCalendarModule } from '@fullcalendar/angular';

import { DashboardDoctorRoutingModule } from './dashboard-doctor-routing.module';
import { DashboardDoctorComponent } from './dashboard-doctor.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { UpdateSchedulesComponent } from './schedules/update-schedules/update-schedules.component';
import { RegisterSchedulesComponent } from './schedules/register-schedules/register-schedules.component';
import { ConfirmEventComponent } from './schedules/confirm-event/confirm-event.component';
import { DeleteSchedulesComponent } from './schedules/delete-schedules/delete-schedules.component';
import { MedicalComponent } from './medical/medical.component';
import { ExamComponent } from './historic/exam/exam.component';
import { CertificateComponent } from './historic/certificate/certificate.component';
import { PrescriptionComponent } from './historic/prescription/prescription.component';
import { MedicalRecordComponent } from './historic/medical-record/medical-record.component';
import { PdfComponent } from './pdf/pdf/pdf.component';

@NgModule({
  declarations: [
    DashboardDoctorComponent,
    ProfileComponent,
    SchedulesComponent,
    UpdateSchedulesComponent,
    RegisterSchedulesComponent,
    ConfirmEventComponent,
    DeleteSchedulesComponent,
    MedicalComponent,
    ExamComponent,
    CertificateComponent,
    PrescriptionComponent,
    MedicalRecordComponent,
    PdfComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardDoctorRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    AngularMultiSelectModule,
    FullCalendarModule,
  ]
})
export class DashboardDoctorModule { }
