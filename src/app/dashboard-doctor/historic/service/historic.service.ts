import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HistoricService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/medical`;
  }

  findCertificateHistoricByPatientId(id: number, page: number): Observable<any> {
    return this.http.get(`${this.url}/certificate/historic/patient/${id}/page/${page}`).pipe(map((result: any) => result));
  }

  findPrescriptionHistoricByPatientId(id: number, page: number): Observable<any> {
    return this.http.get(`${this.url}/prescription/historic/patient/${id}/page/${page}`).pipe(map((result: any) => result));
  }

  findExamHistoricByPatientId(id: number, page: number): Observable<any> {
    return this.http.get(`${this.url}/exam/historic/patient/${id}/page/${page}`).pipe(map((result: any) => result));
  }

  findMedicalRecordHistoricByPatientId(id: number, page: number): Observable<any> {
    return this.http.get(`${this.url}/record/historic/patient/${id}/page/${page}`).pipe(map((result: any) => result));
  }

}
