import { Component, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { HistoricService } from '../service/historic.service';
import { PdfComponent } from '../../pdf/pdf/pdf.component';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css'],
  providers: [HistoricService]
})
export class ExamComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;
  @ViewChild('modalPdf', { static: false }) public modalPdf: PdfComponent;

  page = 0;
  finished = false;
  isModalShown = false;
  patientId: number;
  items: Array<any>;

  constructor(
    private historicService: HistoricService,
    private toaster: ToasterService,
  ) {
    this.items = new Array<any>();
  }

  public findExamHistoricByPatientId() {
    this.page++;

    this.historicService.findExamHistoricByPatientId(this.patientId, this.page).subscribe((result: any) => {
      this.items = result.items;

      // if (result.items.length === 0) {
      //   this.finished = true;
      // } else if (result.items.length < 10) {
      //   this.finished = true;
      //   this.items = this.items.concat(result.items);
      // } else { this.items = this.items.concat(result.items); }
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModalPdf(record: any, data: any) {
    this.modalPdf.openModal('exam', record, data);
  }

  public openModal(patientId: number) {
    this.patientId = patientId;
    this.isModalShown = true;
    this.findExamHistoricByPatientId();
  }

  public closeModal() {
    this.page = 0;
    this.items = new Array<any>();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
