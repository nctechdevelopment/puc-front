import { Component, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { HistoricService } from '../service/historic.service';
import { PdfComponent } from '../../pdf/pdf/pdf.component';

@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./prescription.component.css'],
  providers: [HistoricService]
})
export class PrescriptionComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;
  @ViewChild('modalPdf', { static: false }) public modalPdf: PdfComponent;

  page = 0;
  finished = false;
  isModalShown = false;
  patientId: number;
  items: Array<any>;

  constructor(
    private historicService: HistoricService,
    private toaster: ToasterService,
  ) {
    this.items = new Array<any>();
  }

  public findPrescriptionHistoricByPatientId() {
    this.page++;

    this.historicService.findPrescriptionHistoricByPatientId(this.patientId, this.page).subscribe((result: any) => {
      this.items = result.items;

      // if (result.items.length === 0) {
      //   this.finished = true;
      // } else if (result.items.length < 10) {
      //   this.finished = true;
      //   this.items = this.items.concat(result.items);
      // } else { this.items = this.items.concat(result.items); }
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModalPdf(record: any, data: any) {
    this.modalPdf.openModal('prescription', record, data);
  }

  public openModal(patientId: number) {
    this.patientId = patientId;
    this.isModalShown = true;
    this.findPrescriptionHistoricByPatientId();
  }

  public closeModal() {
    this.page = 0;
    this.items = new Array<any>();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
