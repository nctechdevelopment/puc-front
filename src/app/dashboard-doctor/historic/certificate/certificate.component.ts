import { Component, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { CertificateModel } from '../../../models/certificate.model';
import { HistoricService } from '../service/historic.service';
import { PdfComponent } from '../../pdf/pdf/pdf.component';

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css'],
  providers: [HistoricService]
})
export class CertificateComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;
  @ViewChild('modalPdf', { static: false }) public modalPdf: PdfComponent;

  page = 0;
  finished = false;
  isModalShown = false;
  patientId: number;
  certificates: Array<CertificateModel>;

  constructor(
    private historicService: HistoricService,
    private toaster: ToasterService,
  ) {
    this.certificates = new Array<CertificateModel>();
  }

  public findCertificateHistoricByPatientId() {
    this.page++;

    this.historicService.findCertificateHistoricByPatientId(this.patientId, this.page).subscribe((result: any) => {
      this.certificates = result.certificates;

      // if (result.certificates.length === 0) {
      //   this.finished = true;
      // } else if (result.certificates.length < 10) {
      //   this.finished = true;
      //   this.certificates = this.certificates.concat(result.certificates);
      // } else { this.certificates = this.certificates.concat(result.certificates); }
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModalPdf(record: any, data: any) {
    this.modalPdf.openModal('certificate', record, data);
  }

  public openModal(patientId: number) {
    this.patientId = patientId;
    this.isModalShown = true;
    this.findCertificateHistoricByPatientId();
  }

  public closeModal() {
    this.page = 0;
    this.certificates = new Array<CertificateModel>();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
