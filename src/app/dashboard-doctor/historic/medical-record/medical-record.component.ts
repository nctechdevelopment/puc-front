import { Component, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
moment.locale('pt-BR');

import { MedicalRecordModel } from '../../../models/medical-record.model';
import { HistoricService } from '../service/historic.service';

@Component({
  selector: 'app-medical-record',
  templateUrl: './medical-record.component.html',
  styleUrls: ['./medical-record.component.css'],
  providers: [HistoricService]
})
export class MedicalRecordComponent {

  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  page = 0;
  finished = false;
  isModalShown = false;
  patientId: number;
  records: Array<MedicalRecordModel>;

  constructor(
    private historicService: HistoricService,
    private toaster: ToasterService,
  ) {
    this.records = new Array<MedicalRecordModel>();
  }

  public findMedicalRecordHistoricByPatientId() {
    this.page++;

    this.historicService.findMedicalRecordHistoricByPatientId(this.patientId, this.page).subscribe((result: any) => {
      this.records = result.records;

      // if (result.records.length === 0) {
      //   this.finished = true;
      // } else if (result.records.length < 10) {
      //   this.finished = true;
      //   this.records = this.records.concat(result.records);
      // } else { this.records = this.records.concat(result.records); }
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModal(patientId: number) {
    this.patientId = patientId;
    this.isModalShown = true;
    this.findMedicalRecordHistoricByPatientId();
  }

  public closeModal() {
    this.page = 0;
    this.records = new Array<MedicalRecordModel>();
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
