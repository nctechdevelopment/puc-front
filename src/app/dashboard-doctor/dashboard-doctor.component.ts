import { Component } from '@angular/core';

import { AuthGuard } from '../auth/auth.guard';
import { LoginModel } from '../models/login.model';
import { RefreshTokenService } from '../shared/service/refresh-token.service';

@Component({
  selector: 'app-dashboard-doctor',
  templateUrl: './dashboard-doctor.component.html',
  styleUrls: ['./dashboard-doctor.component.css']
})
export class DashboardDoctorComponent {

  user: LoginModel;

  constructor(
    private authGuard: AuthGuard,
    private refreshTokenService: RefreshTokenService,
  ) {
    this.user = new LoginModel();
    this.user = this.authGuard.getUser();

    this.refreshTokenService.getEventRefreshToken().subscribe(() => this.refreshToken());
  }

  public refreshToken() {
    this.refreshTokenService.refreshToken().subscribe((result: any) => {
      this.authGuard.setToken(result.token);
      this.user = this.authGuard.getUser();
    }, (error) => console.log(error));
  }

}
