import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { ScheduleModel } from 'src/app/models/schedule.model';

@Component({
  selector: 'app-confirm-event',
  templateUrl: './confirm-event.component.html',
  styleUrls: ['./confirm-event.component.css']
})
export class ConfirmEventComponent {

  @Output() emitConfirmEvent: EventEmitter<any> = new EventEmitter();
  @ViewChild('autoShownModal', { static: false }) public autoShownModal: ModalDirective;

  updateStartDate: ScheduleModel;
  updateEndDate: ScheduleModel;
  isModalShown = false;

  constructor() {
    this.updateStartDate = new ScheduleModel();
    this.updateEndDate = new ScheduleModel();
  }

  public confirmEvent() {
    this.emitConfirmEvent.emit(true);
    this.closeModal();
  }

  public cancelEvent() {
    this.emitConfirmEvent.emit(false);
    this.closeModal();
  }

  public openModal(start: ScheduleModel, end: ScheduleModel) {
    this.updateStartDate = start;
    this.updateEndDate = end;
    this.isModalShown = true;
  }

  public closeModal() {
    this.autoShownModal.hide();
  }

  public onHiddenModal() {
    this.isModalShown = false;
  }

}
