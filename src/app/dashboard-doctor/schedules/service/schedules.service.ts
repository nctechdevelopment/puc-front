import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PatientModel } from '../../../models/patient.model';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SchedulesService {

  urlSchedule: string;
  urlPatient: string;

  constructor(private http: HttpClient) {
    this.urlSchedule = `${environment.API_URL}/schedule`;
    this.urlPatient = `${environment.API_URL}/patient`;
  }

  register(data: any): Observable<any> {
    return this.http.post(this.urlSchedule, data).pipe(map((result: any) => result));
  }

  searchAllPatients(data: any): Observable<any> {
    return this.http.post(`${this.urlPatient}/search`, data).pipe(map((result: Array<PatientModel>) => result));
  }

  findAllByDoctor(id: number, start: string, end: string): Observable<any> {
    return this.http.get(`${this.urlSchedule}/${id}/${start}/${end}`).pipe(map((result: any) => result));
  }

  findById(id: string): Observable<any> {
    return this.http.get(`${this.urlSchedule}/${id}`).pipe(map((result: any) => result));
  }

  update(data: any): Observable<any> {
    return this.http.put(`${this.urlSchedule}/${data.id}`, data).pipe(map((result: any) => result));
  }

  updateEventDrop(data: any): Observable<any> {
    return this.http.put(`${this.urlSchedule}/event/${data.id}`, data).pipe(map((result: any) => result));
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.urlSchedule}/${id}`).pipe(map((result: any) => result));
  }

}
