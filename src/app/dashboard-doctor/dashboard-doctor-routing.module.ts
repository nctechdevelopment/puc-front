import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { PermissionGuard } from '../auth/permission.guard';
import { DashboardDoctorComponent } from './dashboard-doctor.component';
import { ProfileComponent } from './profile/profile.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { MedicalComponent } from './medical/medical.component';

// Patients
import { ListPatientsComponent } from '../shared/patients/list-patients/list-patients.component';

const routes: Routes = [
  {
    path: 'painel-medico',
    canActivate: [AuthGuard, PermissionGuard],
    data: { roles: ['3'] },
    component: DashboardDoctorComponent,
    children: [
      {
        path: 'agenda',
        canActivate: [PermissionGuard],
        data: { roles: ['3'] },
        component: SchedulesComponent
      },
      {
        path: 'prontuario/:id',
        canActivate: [PermissionGuard],
        data: { roles: ['3'] },
        component: MedicalComponent
      },
      {
        path: 'listar/pacientes',
        canActivate: [PermissionGuard],
        data: { roles: ['3'] },
        component: ListPatientsComponent
      },
      {
        path: 'perfil',
        canActivate: [PermissionGuard],
        data: { roles: ['3'] },
        component: ProfileComponent
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardDoctorRoutingModule { }
