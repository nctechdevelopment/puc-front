import { TestBed } from '@angular/core/testing';

import { DashboardDoctorService } from './dashboard-doctor.service';

describe('DashboardDoctorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardDoctorService = TestBed.get(DashboardDoctorService);
    expect(service).toBeTruthy();
  });
});
