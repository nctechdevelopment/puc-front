import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardDoctorService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.API_URL}/doctor`;
  }

  update(fd: FormData): Observable<any> {
    return this.http.post(`${this.url}/update`, fd).pipe(map((result: any) => result));
  }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`).pipe(map((result: any) => result));
  }

}
