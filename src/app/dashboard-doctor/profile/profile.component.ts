import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
moment.locale('pt-BR');

import { AuthGuard } from '../../auth/auth.guard';
import { LoginModel } from '../../models/login.model';
import { DoctorModel } from '../../models/doctor.model';
import { DashboardDoctorService } from '../service/dashboard-doctor.service';
import { CepService } from '../../shared/service/cep.service';
import { RefreshTokenService } from '../../shared/service/refresh-token.service';
import { ChangePasswordSafelyComponent } from '../../shared/change-password-safely/change-password-safely.component';
import { Specialties } from '../../shared/datas/specialties';
import { Settings } from '../../shared/settings/settings';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [DashboardDoctorService]
})
export class ProfileComponent implements OnInit {

  @ViewChild('modalChangePasswordSafely', { static: false }) public modalChangePasswordSafely: ChangePasswordSafelyComponent;

  user: LoginModel;
  doctor: DoctorModel;
  selectedFile: File;
  selectedFilename: string;

  dropdownSettings = {};
  specialties: Array<{ id: number, info: string }>;
  selectedSpecialties: Array<{ id: number, info: string }>;

  constructor(
    private authGuard: AuthGuard,
    private cepService: CepService,
    private refreshTokenService: RefreshTokenService,
    private dashboardDoctorService: DashboardDoctorService,
    private toaster: ToasterService,
  ) {
    this.user = new LoginModel();
    this.doctor = new DoctorModel();
    this.specialties = new Array<{ id: number, info: string }>();
    this.selectedSpecialties = new Array<{ id: number, info: string }>();
  }

  ngOnInit() {
    this.user = this.authGuard.getUser();
    this.specialties = Specialties.specialtyTypes;
    this.dropdownSettings = Settings.dropdown;

    this.findById();
  }

  public findById() {
    this.dashboardDoctorService.findById(this.user.id).subscribe((result: any) => {
      this.doctor = result.doctor;
      this.doctor.dataNascimento = moment(this.doctor.dataNascimento).format('DD/MM/YYYY');
      this.selectedSpecialties = JSON.parse(result.doctor.especialidades);
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public update() {
    const fd = new FormData();
    if (this.selectedFile) { fd.append('image', this.selectedFile, this.selectedFile.name); }
    if (this.doctor.imagemAntiga) { fd.append('imagemAntiga', this.doctor.imagemAntiga); }
    fd.append('id', this.doctor.id.toString());
    fd.append('nome', this.doctor.nome);
    fd.append('dataNascimento', this.doctor.dataNascimento);
    fd.append('especialidades', JSON.stringify(this.selectedSpecialties));
    fd.append('cpf', this.doctor.cpf);
    fd.append('rg', this.doctor.rg);
    fd.append('crm', this.doctor.crm);
    fd.append('cep', this.doctor.cep);
    fd.append('estado', this.doctor.estado);
    fd.append('cidade', this.doctor.cidade);
    fd.append('bairro', this.doctor.bairro);
    fd.append('logradouro', this.doctor.logradouro);
    if (this.doctor.complemento) { fd.append('complemento', this.doctor.complemento); }
    fd.append('numero', this.doctor.numero);
    fd.append('celular', this.doctor.celular);
    if (this.doctor.telefone) { fd.append('telefone', this.doctor.telefone); }

    this.dashboardDoctorService.update(fd).subscribe((result: any) => {
      this.toaster.pop('success', 'Sucesso!', result.message);
      this.refreshTokenService.emitEventRefreshToken();
      this.selectedFile = null;
      this.selectedFilename = '';
      this.findById();
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

  public openModalChangePasswordSafely() {
    this.modalChangePasswordSafely.openModal(this.user.loginId);
  }

  public searchCep() {
    this.cepService.searchCep(this.doctor.cep).subscribe((result: any) => {
      if (result.erro) { this.toaster.pop('error', 'Ops!', 'CEP não encontrado.'); }
      this.doctor.complemento = result.complemento ? result.complemento : null;
      this.doctor.logradouro = result.logradouro ? result.logradouro : null;
      this.doctor.bairro = result.bairro ? result.bairro : null;
      this.doctor.cidade = result.localidade ? result.localidade : null;
      this.doctor.estado = result.uf ? result.uf : null;
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', 'CEP inválido.');
    });
  }

  public onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    this.selectedFilename = event.target.files[0].name;
  }

  public onDeSelectAll() {
    this.selectedSpecialties = [];
  }

}
