import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { LoginModel } from '../models/login.model';

@Injectable({
  providedIn: 'root'
})
export class LoginExamService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.BASE_URL;
  }

  loginExam(user: LoginModel): Observable<any> {
    return this.http.post(this.url + '/access/login/exam', user).pipe(map((result: any) => result));
  }

}
