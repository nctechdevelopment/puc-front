import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginExamComponent } from './login-exam.component';

describe('LoginExamComponent', () => {
  let component: LoginExamComponent;
  let fixture: ComponentFixture<LoginExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
