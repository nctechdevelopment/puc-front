import { Component } from '@angular/core';

import { ToasterService } from 'angular2-toaster';

import { AuthGuard } from '../auth/auth.guard';
import { LoginExamService } from './login-exam.service';
import { LoginModel } from '../models/login.model';

@Component({
  selector: 'app-login-exam',
  templateUrl: './login-exam.component.html',
  styleUrls: ['./login-exam.component.css'],
  providers: [LoginExamService]
})
export class LoginExamComponent {

  user: LoginModel;

  constructor(
    private loginExamService: LoginExamService,
    private toaster: ToasterService,
    private authGuard: AuthGuard,
  ) {
    this.user = new LoginModel();
  }

  public loginExam() {
    this.loginExamService.loginExam(this.user).subscribe((result: any) => {
      console.log(result);
      this.toaster.pop('success', 'Sucesso!', 'Login realizado com sucesso!');
      this.authGuard.setToken(result.token);
      window.location.href = '/painel-paciente';
    }, (error) => {
      console.log(error);
      this.toaster.pop('error', 'Ops!', error.error.message);
    });
  }

}
