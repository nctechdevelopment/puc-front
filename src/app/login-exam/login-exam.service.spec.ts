import { TestBed } from '@angular/core/testing';

import { LoginExamService } from './login-exam.service';

describe('LoginExamService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginExamService = TestBed.get(LoginExamService);
    expect(service).toBeTruthy();
  });
});
